# Star Schiff

The purpose of this library is to numerically solve the radiative properties of
soft particles with respect to an "Approximation Method for Short Wavelength or
High-Energy Scattering" (L. Schiff, 1956). The Monte-Carlo Method is used in
order to solve Maxwell's equations within Schiff's approximation, as presented
in
[Charon et al.2015](http://www.sciencedirect.com/science/article/pii/S0022407315003283).
The main advantages of using this method are: the possibility to address any shape
of particle, and the results are provided with a numerical accuracy. The user
has therefore the ability to increase the computation time in order to obtain
more accurate results if needed.

Monte-Carlo is used to estimates total cross-sections (absorption, scattering
and extinction cross-sections) in addition to the phase function, its
cumulative and its inverse cumulative, for a mixture of several particles.
These set of particles is defined by its optical properties (refractive index,
provided at various wavelengths) and a geometry distribution that controls the
shape of the particles.

## How to build

The Star-Schiff library relies on the [CMake](http://www.cmake.org) and the
[RCMake](https://gitlab.com/vaplv/rcmake/) package to build. It also depends on the
[GNU Scientific Library](http://www.gnu.org/software/gsl/),
[RSys](https://gitlab.com/vaplv/rsys/),
[Star-3D](https://gitlab.com/meso-star/star-3d/) and
[Star-SP](https://gitlab.com/meso-star/star-sp/) libraries.

First ensure that CMake is installed on your system. Then install the RCMake
package as well as all the aforementioned Star-Schiff prerequisites. Then
generate the project from the `cmake/CMakeLists.txt` file by appending to the
`CMAKE_PREFIX_PATH` variable the install directories of its dependencies.

## Release notes

### Version 0.4.1

Sets the required version of Star-SampPling to 0.12. This version fixes
compilation errors with gcc 11 but introduces API breaks.

### Version 0.4

- Update the `struct sschiff_geometry_distribution` data structure. The
  `sample` function now only samples the particle geometry. Its volume scaling
  is sampled through the new `sample_volume_scaling` function. This scaling
  factor is now sampled at the same frequency than the particle orientation,
  i.e. several times per sampled particle geometry.
- Relax constraints on the minimum number of scattering angles. It is now set
  to 2 rather than 3, i.e. the scattering can be purely forward or backward.
- Overall update of the project dependencies. Most notably, the update of the
  Star-3D library drastically improves the performances of sampling particles
  with the same shape.
- Add an option to avoid the analytic computations of wide angles.

## Copying

Copyright (C) 2020, 2021 |Meso|Star> (<contact@meso-star.com>).  
Copyright (C) 2015, 2016 CNRS.

Star-Schiff is free software released under the GPL v3+ license: GNU GPL
version 3 or later.  You are welcome to redistribute it under certain
conditions; refer to the COPYING file for details.

