/* Copyright (C) 2020, 2021 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2015, 2016 CNRS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "sschiff.h"
#include "sschiff_device.h"

#include <rsys/logger.h>
#include <rsys/mem_allocator.h>
#include <rsys/stretchy_array.h>

#include <star/s3d.h>

#include <stdarg.h>

#include <omp.h>

/*******************************************************************************
 * Helper function
 ******************************************************************************/
static void
device_release(ref_T* ref)
{
  struct sschiff_device* dev;
  ASSERT(ref);
  dev = CONTAINER_OF(ref, struct sschiff_device, ref);
  if(dev->s3d) {
    size_t i;
    FOR_EACH(i, 0, dev->nthreads) {
      if(dev->s3d[i]) S3D(device_ref_put(dev->s3d[i]));
    }
    sa_release(dev->s3d);
  }
  MEM_RM(dev->allocator, dev);
}

/*******************************************************************************
 * Local functions
 ******************************************************************************/
void
log_error(struct sschiff_device* dev, const char* msg, ...)
{
  va_list vargs_list;
  ASSERT(dev && msg);
  if(dev->verbose) {
    res_T res; (void)res;
    va_start(vargs_list, msg);
    res = logger_vprint(dev->logger, LOG_ERROR, msg, vargs_list);
    ASSERT(res == RES_OK);
    va_end(vargs_list);
  }
}

void
log_warning(struct sschiff_device* dev, const char* msg, ...)
{
  va_list vargs_list;
  ASSERT(dev && msg);
  if(dev->verbose) {
    res_T res; (void)res;
    va_start(vargs_list, msg);
    res = logger_vprint(dev->logger, LOG_WARNING, msg, vargs_list);
    ASSERT(res == RES_OK);
    va_end(vargs_list);
  }
}

/*******************************************************************************
 * API functions
 ******************************************************************************/
res_T
sschiff_device_create
  (struct logger* log,
   struct mem_allocator* allocator,
   const unsigned nthreads_hint,
   const int verbose,
   struct s3d_device* s3d,
   struct sschiff_device** out_dev)
{
  struct mem_allocator* mem_allocator;
  struct sschiff_device* dev = NULL;
  struct logger* logger = NULL;
  size_t i;
  res_T res = RES_OK;

  if(!out_dev || !nthreads_hint) {
    res = RES_BAD_ARG;
    goto error;
  }

  mem_allocator = allocator ? allocator : &mem_default_allocator;
  logger = log ? log : LOGGER_DEFAULT;

  dev = MEM_CALLOC(mem_allocator, 1, sizeof(struct sschiff_device));
  if(!dev) {
    log_error(dev, "Couldn't allocate the Star-Schiff device.\n");
    res = RES_MEM_ERR;
    goto error;
  }
  ref_init(&dev->ref);
  dev->allocator = mem_allocator;
  dev->logger = logger;
  dev->verbose = verbose;
  dev->nthreads = MMIN(nthreads_hint, (unsigned)omp_get_num_procs());

  if(!sa_add(dev->s3d, dev->nthreads)) {
    log_error(dev,
      "Not enough memory: couldn't allocate the list of S3D devices.\n");
    res = RES_MEM_ERR;
    goto error;
  }
  memset(dev->s3d, 0, sizeof(struct s3d_device*)*dev->nthreads);

  FOR_EACH(i, 0, dev->nthreads) {
    if(s3d) {
      S3D(device_ref_get(s3d));
      dev->s3d[i] = s3d;
    } else {
      /* Create as many Star-3D devices as threads since this is actually more
       * efficient. It seems that the Star-3D back-end (i.e. embree) shared
       * some ressources - and thus perform some synchronisations leading to
       * performance loss - if several acceleration data structures are build
       * on the same device. */
      res = s3d_device_create(dev->logger, dev->allocator, 0, &dev->s3d[i]);
      if(res != RES_OK) {
        log_error
          (dev, "Couldn't create the internal Star-3D device of Star-Schiff.\n");
        goto error;
      }
    }
  }

  omp_set_num_threads((int)dev->nthreads);

exit:
  if(out_dev) *out_dev = dev;
  return res;
error:
  if(dev) {
    SSCHIFF(device_ref_put(dev));
    dev = NULL;
  }
  goto exit;
}

res_T
sschiff_device_get_threads_count(struct sschiff_device* dev, unsigned* nthreads)
{
  if(!dev || !nthreads) return RES_BAD_ARG;
  *nthreads = dev->nthreads;
  return RES_OK;
}

res_T
sschiff_device_ref_get(struct sschiff_device* dev)
{
  if(!dev) return RES_BAD_ARG;
  ref_get(&dev->ref);
  return RES_OK;
}

res_T
sschiff_device_ref_put(struct sschiff_device* dev)
{
  if(!dev) return RES_BAD_ARG;
  ref_put(&dev->ref, device_release);
  return RES_OK;
}

