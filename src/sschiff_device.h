/* Copyright (C) 2020, 2021 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2015, 2016 CNRS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef SSCHIFF_DEVICE_H
#define SSCHIFF_DEVICE_H

#include <rsys/ref_count.h>

struct logger;
struct mem_allocator;
struct s3d_device;

struct sschiff_device {
  int verbose;
  unsigned nthreads;
  struct logger* logger;
  struct s3d_device** s3d;

  struct mem_allocator* allocator;
  ref_T ref;
};

extern LOCAL_SYM void
log_error
  (struct sschiff_device* dev,
   const char* msg,
   ...)
#ifdef COMPILER_GCC
  __attribute((format(printf, 2, 3)))
#endif
;

extern LOCAL_SYM void
log_warning
  (struct sschiff_device* dev,
   const char* msg,
   ...)
#ifdef COMPILER_GCC
  __attribute((format(printf, 2, 3)))
#endif
;

#endif /* SSCHIFF_DEVICE_H */

