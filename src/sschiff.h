/* Copyright (C) 2020, 2021 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2015, 2016 CNRS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef SSCHIFF_H
#define SSCHIFF_H

#include <rsys/rsys.h>

/* Library symbol management */
#if defined(SSCHIFF_SHARED_BUILD) /* Build shared library */
  #define SSCHIFF_API extern EXPORT_SYM
#elif defined(SSCHIFF_STATIC) /* Use/build static library */
  #define SSCHIFF_API extern LOCAL_SYM
#else /* Use shared library */
  #define SSCHIFF_API extern IMPORT_SYM
#endif

/* Helper macro that asserts if the invocation of the schiff function `Func'
 * returns an error. One should use this macro on sschiff function calls for
 * which no explicit error checking is performed */
#ifndef NDEBUG
  #define SSCHIFF(Func) ASSERT(sschiff_ ## Func == RES_OK)
#else
  #define SSCHIFF(Func) sschiff_ ## Func
#endif

#define SSCHIFF_NTHREADS_DEFAULT (~0u)

/* Forward declaration of external types. */
struct mem_allocator;
struct logger;
struct s3d_device;
struct s3d_shape;
struct ssp_rng;

/* Optical properties of a material handled by the Schiff integrator */
struct sschiff_material_properties {
  double medium_refractive_index;
  double relative_real_refractive_index;
  double relative_imaginary_refractive_index;
};

/* Client side material used by the Schiff integrator */
struct sschiff_material {
  /* Retrieve the optical properties of the material */
  void (*get_property)
    (void* material,
     const double wavelength, /* In micron */
     struct sschiff_material_properties* properties);
  void* material; /* Opaque user defined representation of the material */
};

static const struct sschiff_material SSCHIFF_NULL_MATERIAL = { NULL, NULL };

/* User defined distribution of the geometries. The unit of the geometry is the
 * micron, i.e. 1.0f == 1 micron */
struct sschiff_geometry_distribution {
  struct sschiff_material material; /* Material of the geometry distribution */
  double characteristic_length;
  res_T (*sample) /* Sample a geometry i.e. a shape */
    (struct ssp_rng* rng,
     void** shape, /* Returned client side data of the sampled shape */
     struct s3d_shape* s3d_shape, /* Star-3D shape where geometry are stored */
     void* context);
  res_T (*sample_volume_scaling) /* Can be NULL <=> No volume scaling */
    (struct ssp_rng* rng,
     void* shape, /* Client side shape into which a volume scaling is sampled */
     double* scale_factor, /* Scale factor to apply to the shape volume */
     void* context);
  void* context;
};

static const struct sschiff_geometry_distribution
SSCHIFF_NULL_GEOMETRY_DISTRIBUTION = { {NULL, NULL}, -1.0, NULL, NULL, NULL };

/* State of the Monte Carlo estimation */
struct sschiff_state {
  double E; /* Expected value */
  double V; /* Variance */
  double SE; /* Standard error */
};

struct sschiff_cross_section {
  struct sschiff_state extinction;
  struct sschiff_state absorption;
  struct sschiff_state scattering;
  struct sschiff_state average_projected_area;
};

/* Type of the function used to distribute the scattering angles in [0, PI].
 * nangles is assumed to be greater than or equal to 2 and the angles is
 * ensured to have `nangles' entries. */
typedef void (*sschiff_scattering_angles_distribution_T)
  (double angles[], const size_t nangles);

/* Opaque types */
struct sschiff_device;
struct sschiff_estimator;

BEGIN_DECLS

/* Built-in uniform distribution of scattering angles */
SSCHIFF_API const sschiff_scattering_angles_distribution_T
sschiff_uniform_scattering_angles;

/*******************************************************************************
 * Star Schiff API
 ******************************************************************************/

SSCHIFF_API res_T
sschiff_device_create
  (struct logger* logger, /* May be NULL <=> use default logger */
   struct mem_allocator* allocator, /* May be NULL <=> use default allocator */
   const unsigned nthreads_hint, /* Hint on the number of threads to use */
   const int verbose, /* Make the command more verbose */
   struct s3d_device* s3d, /* May be NULL <=> internally create a S3D device */
   struct sschiff_device** dev);

/* Return the number of threads effectively used by the Star-Schiff device */
SSCHIFF_API res_T
sschiff_device_get_threads_count
  (struct sschiff_device* dev,
   unsigned* nthreads);

SSCHIFF_API res_T
sschiff_device_ref_get
  (struct sschiff_device* dev);

SSCHIFF_API res_T
sschiff_device_ref_put
  (struct sschiff_device* dev);

SSCHIFF_API res_T
sschiff_integrate
  (struct sschiff_device* dev,
   struct ssp_rng* rng,
   struct sschiff_geometry_distribution* distribution,
   /* Wavelengths to estimate in micron. Must be sorted in ascending order. */
   const double* wavelengths,
   const size_t wavelengths_count, /* # wavelengths to estimate */
   const sschiff_scattering_angles_distribution_T angles, /* angles distrib */
   const size_t scattering_angles_count, /* # scattering angles. Must be >= 3 */
   const size_t sampled_geometries_count, /* # geometries to sample */
   const size_t sampled_directions_count, /* # directions to sample per geometry */
   const int discard_wide_angles, /* Avoid analytic model for wide angles */
   struct sschiff_estimator** estimator);

SSCHIFF_API res_T
sschiff_estimator_ref_get
  (struct sschiff_estimator* estimator);

SSCHIFF_API res_T
sschiff_estimator_ref_put
  (struct sschiff_estimator* estimator);

/* Return the list of estimated wavelengths. */
SSCHIFF_API res_T
sschiff_estimator_get_wavelengths
  (const struct sschiff_estimator* estimator,
   const double** wavelengths, /* May be NULL, i.e. do not return this list */
   size_t* count); /* May be NULL, i.e. do not return the # wavelengths */

/* Return the list of scattering angles. */
SSCHIFF_API res_T
sschiff_estimator_get_scattering_angles
  (const struct sschiff_estimator* estimator,
   const double** angles, /* May be NULL, i.e. do not return the angles */
   size_t* count); /* May be NULL, i.e. do not return the # scattering angles */

/* Return the number of Monte Carlo realisations */
SSCHIFF_API res_T
sschiff_estimator_get_realisations_count
  (const struct sschiff_estimator* estimator,
   size_t* count);

/* Retrieve the estimation state of a given wavelength. */
SSCHIFF_API res_T
sschiff_estimator_get_cross_section
  (const struct sschiff_estimator* estimator,
   const size_t wavelength_index, /* Id of the wavelengths */
   struct sschiff_cross_section* cross_section);

/* Retrieve the estimated cross sections of all wavelengths. The length of
 * `cross_sections' must be at least equal to the count of integrated
 * wavelengths. One can use the sschiff_estimator_get_wavelengths function to
 * retrieve this information. */
SSCHIFF_API res_T
sschiff_estimator_get_cross_sections
  (const struct sschiff_estimator* estimator,
   struct sschiff_cross_section cross_sections[]);

/* Retrieve a pointer onto the estimated phase function for the scattering
 * angles of the estimator. Use the sschiff_estimator_get_scattering_angles
 * function to retrieve these scattering angles. Return RES_BAD_OP if the phase
 * function couldn't be estimated for this wavelength */
SSCHIFF_API res_T
sschiff_estimator_get_phase_function
  (const struct sschiff_estimator* estimator,
   const size_t wavelength_index,
   const struct sschiff_state** states);

/* Retrieve a pointer onto the estimated phase function cumulative for the
 * scattering angles of the estimator. Use the
 * sschiff_estimator_get_scattering_angles function to retrieve these
 * scattering angles. Return RES_BAD_OP if the phase function couldn't be
 * estimated for this wavelength */
SSCHIFF_API res_T
sschiff_estimator_get_phase_function_cumulative
  (const struct sschiff_estimator* estimator,
   const size_t wavelength_index,
   const struct sschiff_state** states);

/* Compute the inverse cumulative of the estimated phase function. The inverse
 * cumulative is computed for a set of `nthetas' values distributed in [0, 1]
 * as [0, 1*S, 2*S, ..., i*S, ..., (nthetas-1)*S] with S=1/(nthetas-1). The
 * ouput array `thetas' stored the angles corresponding to these cumulative
 * values. Its length must be at least equal to `nthetas'. Return RES_BAD_OP of
 * the cumulative phase function cannot be inverted. */
SSCHIFF_API res_T
sschiff_estimator_inverse_cumulative_phase_function
  (const struct sschiff_estimator* estimator,
   const size_t wavelength_index,
   double thetas[],
   const size_t nthetas); /* Must be >= 2 */


/* Retrieve, for the submitted wavelength, the index of the last small
 * scattering angle, i.e. the last angle from which the phase function
 * [cumulative] is estimated by Monte Carlo. One can use this index to get the
 * value of the limit angle from the scattering angles returned by the
 * sschiff_estimator_get_scattering_angles function. Return RES_BAD_OP if the
 * phase function is not computable for this wavelength. */
SSCHIFF_API res_T
sschiff_estimator_get_limit_scattering_angle_index
  (const struct sschiff_estimator* estimator,
   const size_t wavelength_index,
   size_t* scattering_angle_index);

/* Return the `n' parameter of the model used to compute the phase function of
 * the wide scattering angles for a given wavelength. Return RES_BAD_OP if the
 * phase function is not computable for this wavelength */
SSCHIFF_API res_T
sschiff_estimator_get_wide_scattering_angle_model_parameter
  (const struct sschiff_estimator* estimator,
   const size_t wavelength_index,
   double* n);

/* Return the estimated differential cross section for a wavelength at a given
 * scattering angle. Return RES_BAD_OP if it was not computable. */
SSCHIFF_API res_T
sschiff_estimator_get_differential_cross_section
  (const struct sschiff_estimator* estimator,
   const size_t wavelength_index,
   const size_t scattering_angle_index,
   struct sschiff_state* state);

/* Return the estimated differential cross section cumulative for a wavelength
 * at a given scattering angle. Return RES_BAD_OP if it was not computable. */
SSCHIFF_API res_T
sschiff_estimator_get_differential_cross_section_cumulative
  (const struct sschiff_estimator* estimator,
   const size_t wavelength_index,
   const size_t scanttering_angle_index,
   struct sschiff_state* state);

END_DECLS

#endif /* SSCHIFF_H */

