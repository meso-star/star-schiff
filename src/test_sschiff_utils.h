/* Copyright (C) 2020, 2021 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2015, 2016 CNRS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef TEST_SSCHIFF_UTILS_H
#define TEST_SSCHIFF_UTILS_H

#include <rsys/float3.h>
#include <rsys/stretchy_array.h>
#include <rsys/mem_allocator.h>

#include <star/s3d.h>

/*******************************************************************************
 * Helper geometry data structure
 ******************************************************************************/
struct geometry {
  float* vertices; /* List of float[3] */
  unsigned* indices; /* List of unsigned[3] */
};

static INLINE void
geometry_init_sphere(struct geometry* sphere, const unsigned ntheta)
{
  const unsigned nphi = (unsigned)(((double)ntheta + 0.5) / 2.0);
  const double step_theta = 2*PI / (double)ntheta;
  const double step_phi = PI / (double)nphi;
  double* cos_theta = NULL;
  double* sin_theta = NULL;
  double* cos_phi = NULL;
  double* sin_phi = NULL;
  unsigned itheta, iphi;

  CHK(sphere != NULL);
  CHK(ntheta != 0);

  sphere->vertices = NULL;
  sphere->indices = NULL;

  /* Precompute the cosine/sinus of the theta/phi angles */
  FOR_EACH(itheta, 0, ntheta) {
    const double theta = -PI + (double)itheta * step_theta;
    sa_push(cos_theta, cos(theta));
    sa_push(sin_theta, sin(theta));
  }
  FOR_EACH(iphi, 1, nphi) {
    const double phi = -PI/2 + (double)iphi * step_phi;
    sa_push(cos_phi, cos(phi));
    sa_push(sin_phi, sin(phi));
  }
  /* Compute the contour vertices */
  FOR_EACH(itheta, 0, ntheta) {
    FOR_EACH(iphi, 0, nphi-1) {
      sa_push(sphere->vertices, (float)(cos_theta[itheta]*cos_phi[iphi]));
      sa_push(sphere->vertices, (float)(sin_theta[itheta]*cos_phi[iphi]));
      sa_push(sphere->vertices, (float)sin_phi[iphi]);
    }
  }
  /* Compute the polar vertices */
  f3(sa_add(sphere->vertices, 3), 0.f, 0.f,-1.f);
  f3(sa_add(sphere->vertices, 3), 0.f, 0.f, 1.f);

  /* Define the indices of the  contour primitives */
  FOR_EACH(itheta, 0, ntheta) {
    const unsigned itheta0 = itheta * (nphi - 1);
    const unsigned itheta1 = ((itheta + 1) % ntheta) * (nphi - 1);
    FOR_EACH(iphi, 0, nphi-2) {
      const unsigned iphi0 = iphi + 0;
      const unsigned iphi1 = iphi + 1;

      sa_push(sphere->indices, itheta0 + iphi0);
      sa_push(sphere->indices, itheta0 + iphi1);
      sa_push(sphere->indices, itheta1 + iphi0);

      sa_push(sphere->indices, itheta1 + iphi0);
      sa_push(sphere->indices, itheta0 + iphi1);
      sa_push(sphere->indices, itheta1 + iphi1);
    }
  }

  /* Define the indices of the polar primitives */
  FOR_EACH(itheta, 0, ntheta) {
    const unsigned itheta0 = itheta * (nphi - 1);
    const unsigned itheta1 = ((itheta + 1) % ntheta) * (nphi - 1);

    sa_push(sphere->indices, ntheta * (nphi - 1));
    sa_push(sphere->indices, itheta0);
    sa_push(sphere->indices, itheta1);

    sa_push(sphere->indices, ntheta * (nphi - 1) + 1);
    sa_push(sphere->indices, itheta1 + (nphi - 2));
    sa_push(sphere->indices, itheta0 + (nphi - 2));
  }

  /* Release the intermediary data structure */
  sa_release(cos_theta);
  sa_release(sin_theta);
  sa_release(cos_phi);
  sa_release(sin_phi);
}

static INLINE void
geometry_init_cylinder(struct geometry* geometry, const unsigned nsteps)
{
  const double step = 2*PI / (double)nsteps;
  unsigned istep;

  CHK(geometry != NULL);
  CHK(nsteps != 0);

  geometry->vertices = NULL;
  geometry->indices = NULL;

  /* Generate the vertex coordinates */
  FOR_EACH(istep, 0, nsteps) {
    const float theta = (float)(istep * step);
    const float x = (float)cos(theta);
    const float y = (float)sin(theta);
    f3(sa_add(geometry->vertices, 3), x, y, -1.f);
    f3(sa_add(geometry->vertices, 3), x, y, 0.f);
  }

  /* "Polar" vertices */
  f3(sa_add(geometry->vertices, 3), 0.f, 0.f, -1.f);
  f3(sa_add(geometry->vertices, 3), 0.f, 0.f, 0.f);

  /* Contour primitives */
  FOR_EACH(istep, 0, nsteps) {
    const unsigned id = istep * 2;
    unsigned* iprim;

    iprim = sa_add(geometry->indices, 3);
    iprim[0] = (id + 0);
    iprim[1] = (id + 1);
    iprim[2] = (id + 2) % (nsteps*2);

    iprim = sa_add(geometry->indices, 3);
    iprim[0] = (id + 2) % (nsteps*2);
    iprim[1] = (id + 1);
    iprim[2] = (id + 3) % (nsteps*2);
  }

  /* Cap primitives */
  FOR_EACH(istep, 0, nsteps) {
    const unsigned id = istep * 2;
    unsigned* iprim;

    iprim = sa_add(geometry->indices, 3);
    iprim[0] = (nsteps * 2);
    iprim[1] = (id + 0);
    iprim[2] = (id + 2) % (nsteps*2);

    iprim = sa_add(geometry->indices, 3);
    iprim[0] = (nsteps * 2) + 1;
    iprim[1] = (id + 3) % (nsteps*2);
    iprim[2] = (id + 1);
  }

}

static INLINE void
geometry_release(struct geometry* geometry)
{
  CHK(geometry != NULL);
  sa_release(geometry->vertices);
  sa_release(geometry->indices);
  geometry->vertices = NULL;
  geometry->indices = NULL;
}

static INLINE void
geometry_dump(struct geometry* geom, FILE* file)
{
  size_t i;
  CHK(geom != NULL);
  CHK(file != NULL);

  CHK(sa_size(geom->vertices)%3 == 0); /* Ensure 3D position */
  CHK(sa_size(geom->indices)%3 == 0); /* Ensure triangular primitives */

  FOR_EACH(i, 0, sa_size(geom->vertices)/3) {
    fprintf(file, "v %f %f %f\n",
      geom->vertices[i*3+0],
      geom->vertices[i*3+1],
      geom->vertices[i*3+2]);
  }
  FOR_EACH(i, 0, sa_size(geom->indices)/3) {
    fprintf(file, "f %d %d %d\n",
      geom->indices[i*3+0] + 1,
      geom->indices[i*3+1] + 1,
      geom->indices[i*3+2] + 1);
  }
}

/*******************************************************************************
 * Cylinder shape
 ******************************************************************************/
struct cylinder {
  struct geometry* geometry;
  float radius;
  float height;
};

static INLINE void
cylinder_get_indices(const unsigned itri, unsigned ids[3], void* ctx)
{
  struct cylinder* cylinder = ctx;
  const size_t i = itri * 3;

  CHK(sa_size(cylinder->geometry->indices) % 3 == 0);
  CHK(itri < sa_size(cylinder->geometry->indices) / 3);
  ids[0] = cylinder->geometry->indices[i + 0];
  ids[1] = cylinder->geometry->indices[i + 1];
  ids[2] = cylinder->geometry->indices[i + 2];
}

static INLINE void
cylinder_get_position(const unsigned ivert, float vertex[3], void* ctx)
{
  struct cylinder* cylinder = ctx;
  const size_t i = ivert * 3;

  CHK(sa_size(cylinder->geometry->vertices) % 3 == 0);
  CHK(ivert < sa_size(cylinder->geometry->vertices) / 3);
  vertex[0] = cylinder->geometry->vertices[i + 0] * cylinder->radius;
  vertex[1] = cylinder->geometry->vertices[i + 1] * cylinder->radius;
  vertex[2] = cylinder->geometry->vertices[i + 2] * cylinder->height;
}

static INLINE res_T
cylinder_setup_s3d_shape(struct cylinder* cylinder, struct s3d_shape* shape)
{
  struct s3d_vertex_data attrib;
  size_t nverts, nprims;

  CHK(cylinder != NULL);
  CHK(shape != NULL);

  attrib.usage = S3D_POSITION;
  attrib.type = S3D_FLOAT3;
  attrib.get = cylinder_get_position;

  nverts = sa_size(cylinder->geometry->vertices) / 3/*#coords*/;
  nprims = sa_size(cylinder->geometry->indices) / 3/*#indices per prim*/;

  return s3d_mesh_setup_indexed_vertices(shape, (unsigned)nprims,
    cylinder_get_indices, (unsigned)nverts, &attrib, 1, cylinder);
}

/*******************************************************************************
 * Spherical shape
 ******************************************************************************/
struct sphere {
  struct geometry* geometry;
  float radius;
};

static INLINE void
sphere_get_indices(const unsigned itri, unsigned ids[3], void* ctx)
{
  struct sphere* sphere = ctx;
  const size_t i = itri * 3;

  CHK(sa_size(sphere->geometry->indices) % 3 == 0);
  CHK(itri < sa_size(sphere->geometry->indices) / 3);
  ids[0] = sphere->geometry->indices[i + 0];
  ids[1] = sphere->geometry->indices[i + 1];
  ids[2] = sphere->geometry->indices[i + 2];
}

static INLINE void
sphere_get_position(const unsigned ivert, float vertex[3], void* ctx)
{
  struct sphere* sphere = ctx;
  const size_t i = ivert * 3;

  CHK(sa_size(sphere->geometry->vertices) % 3 == 0);
  CHK(ivert < sa_size(sphere->geometry->vertices) / 3);
  vertex[0] = sphere->geometry->vertices[i + 0] * sphere->radius;
  vertex[1] = sphere->geometry->vertices[i + 1] * sphere->radius;
  vertex[2] = sphere->geometry->vertices[i + 2] * sphere->radius;
}

static INLINE res_T
sphere_setup_s3d_shape(struct sphere* sphere, struct s3d_shape* shape)
{
  struct s3d_vertex_data attrib;
  size_t nverts, nprims;

  CHK(sphere != NULL);
  CHK(shape != NULL);

  attrib.usage = S3D_POSITION;
  attrib.type = S3D_FLOAT3;
  attrib.get = sphere_get_position;

  nverts = sa_size(sphere->geometry->vertices) / 3/*#coords*/;
  nprims = sa_size(sphere->geometry->indices) / 3/*#indices per prim*/;

  return s3d_mesh_setup_indexed_vertices(shape, (unsigned)nprims,
    sphere_get_indices, (unsigned)nverts, &attrib, 1, sphere);
}

/*******************************************************************************
 * Miscellaneous functions
 ******************************************************************************/
static INLINE void
compute_estimation_intersection
  (double intersection[2],
   const double scale,
   const struct sschiff_state* state0,
   const struct sschiff_state* state1)
{
  double interval0[2], interval1[2];
  CHK(scale > 0);
  interval0[0] = state0->E - scale*state0->SE;
  interval0[1] = state0->E + scale*state0->SE;
  interval1[0] = state1->E - scale*state1->SE;
  interval1[1] = state1->E + scale*state1->SE;
  intersection[0] = MMAX(interval0[0], interval1[0]);
  intersection[1] = MMIN(interval0[1], interval1[1]);
}

static void
check_memory_allocator(struct mem_allocator* allocator)
{
  if(MEM_ALLOCATED_SIZE(allocator)) {
    char dump[512];
    MEM_DUMP(allocator, dump, sizeof(dump)/sizeof(char));
    fprintf(stderr, "%s\n", dump);
    FATAL("Memory leaks\n");
  }
}

#endif /* TEST_SSCHIFF_UTILS_H */

