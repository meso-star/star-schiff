/* Copyright (C) 2020, 2021 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2015, 2016 CNRS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "sschiff.h"
#include "test_sschiff_utils.h"

#include <rsys/clock_time.h>
#include <rsys/float3.h>
#include <rsys/stretchy_array.h>

#include <star/s3d.h>
#include <star/ssp.h>

struct result {
  double mean_radius; /* In micron */
  double absorption_cross_section;
  double scattering_cross_section;
  double extinction_cross_section;
};

struct sampler_context {
  struct geometry geometry;
  double wavelength; /* In micron */
  double medium_refractive_index;
  double relative_real_refractive_index;
  double relative_imaginary_refractive_index;
  double mean_radius; /* In micron */
  double sigma;
};

static void
get_material_property
  (void* mtl,
   const double wavelength,
   struct sschiff_material_properties* properties)
{
  struct sampler_context* ctx = mtl;

  CHK(mtl != NULL);
  CHK(properties != NULL);
  CHK(eq_eps(ctx->wavelength, wavelength, 1.e-8) == 1);

  properties->medium_refractive_index = ctx->medium_refractive_index;
  properties->relative_real_refractive_index =
    ctx->relative_real_refractive_index;
  properties->relative_imaginary_refractive_index =
    ctx->relative_imaginary_refractive_index;
}

static res_T
sample_sphere
  (struct ssp_rng* rng,
   void** shape_data,
   struct s3d_shape* shape,
   void* sampler_context)
{
  struct sampler_context* sampler_ctx = sampler_context;
  struct sphere sphere;
  CHK(rng && shape_data);

  sphere.geometry = &sampler_ctx->geometry;
  sphere.radius = (float)ssp_ran_lognormal
    (rng, log(sampler_ctx->mean_radius), log(sampler_ctx->sigma));

  *shape_data = NULL;

  return sphere_setup_s3d_shape(&sphere, shape);
}

static void
check_schiff_estimation
  (struct sschiff_device* dev,
   struct ssp_rng* rng,
   struct sampler_context* sampler_ctx,
   const struct result* results,
   const size_t nresults)
{
  char buf[64];
  struct sschiff_estimator* estimator;
  struct sschiff_geometry_distribution distrib = SSCHIFF_NULL_GEOMETRY_DISTRIBUTION;
  struct sschiff_cross_section cross_section;
  struct time t0, t1;
  size_t i;

  const size_t nscatt_angles = 1000;
  const size_t ngeoms = 100;
  const size_t ndirs = 100;
  const double wavelength = sampler_ctx->wavelength;

  CHK(sampler_ctx != NULL);
  CHK(results != NULL);
  CHK(results != 0);

  distrib.material.get_property = get_material_property;
  distrib.material.material = sampler_ctx;
  distrib.sample = sample_sphere;
  distrib.context = sampler_ctx;

  printf("Schiff sphere estimation - m: %g; n_r: %g; kappa_r: %g; n_e: %g\n",
    sampler_ctx->wavelength,
    sampler_ctx->relative_real_refractive_index,
    sampler_ctx->relative_imaginary_refractive_index,
    sampler_ctx->medium_refractive_index);

  FOR_EACH(i, 0, nresults) {
    const struct sschiff_state* val;
    double dst;

    sampler_ctx->mean_radius = results[i].mean_radius;
    distrib.characteristic_length = sampler_ctx->mean_radius * 2.0;

    time_current(&t0);
    CHK(sschiff_integrate(dev, rng, &distrib, &wavelength, 1,
      sschiff_uniform_scattering_angles, nscatt_angles, ngeoms, ndirs, 0, &estimator)
    == RES_OK);
    time_current(&t1);
    time_sub(&t0, &t1, &t0);
    time_dump(&t0, TIME_MIN|TIME_SEC|TIME_MSEC, NULL, buf, sizeof(buf));
    printf("%d - mean radius: %5.2f micron - %s: \n", (int)i, results[i].mean_radius, buf);

    CHK(sschiff_estimator_get_cross_sections(estimator, &cross_section) == RES_OK);

    val = &cross_section.extinction;
    dst = val->E - results[i].extinction_cross_section;
    printf("  Extinction cross section = %7.2f ~ %12.7f +/- %12.7f (%6.2f)\n",
      results[i].extinction_cross_section, val->E, val->SE, dst / val->SE);
    CHK(eq_eps(val->E, results[i].extinction_cross_section, 4*val->SE) == 1);

    val = &cross_section.absorption;
    dst = val->E - results[i].absorption_cross_section;
    printf("  Absorption cross section = %7.2f ~ %12.7f +/- %12.7f (%6.2f)\n",
      results[i].absorption_cross_section, val->E, val->SE, dst / val->SE);
    CHK(eq_eps(val->E, results[i].absorption_cross_section, 4*val->SE) == 1);

    val = &cross_section.scattering;
    dst = val->E - results[i].scattering_cross_section;
    printf("  Scattering cross section = %7.2f ~ %12.7f +/- %12.7f (%6.2f)\n",
      results[i].scattering_cross_section, val->E, val->SE, dst / val->SE);
    CHK(eq_eps(val->E, results[i].scattering_cross_section, 4*val->SE) == 1);

    val = &cross_section.average_projected_area;
    printf("  Averavege projected area = %12.7g +/- %12.7g\n", val->E, val->SE);

    CHK(sschiff_estimator_ref_put(estimator) == RES_OK);
  }
}

int
main(int argc, char** argv)
{
  struct mem_allocator allocator;
  struct sampler_context sampler_ctx;
  struct sschiff_device* dev;
  struct sschiff_geometry_distribution distrib = SSCHIFF_NULL_GEOMETRY_DISTRIBUTION;
  struct sschiff_estimator* estimator;
  struct sschiff_cross_section cross_section;
  struct ssp_rng* rng;
  const struct result results_n_r_1_01[] = {
    { 1.00, 0.484, 0.159, 0.643 },
    { 3.22, 13.4,  13.2,  26.6 },
    { 5.44, 54.6,  79.6,  134 },
    { 7.67, 131,   225,   356 },
    { 9.89, 243,   440,   683 },
    { 12.1, 393,   691,   1080 },
    { 14.3, 578,   951,   1530 },
    { 16.6, 799,   1200,  2000 },
    { 18.8, 1060,  1460,  2510 },
    { 21.0, 1350,  1730,  3070 }
  };
  const struct result results_n_r_1_1[] = {
    { 1.00, 0.484, 7.90, 8.39 },
    { 3.22, 13.4,  55.9, 69.3 },
    { 5.44, 54.6,  143,  197 },
    { 7.67, 131,   261,  392 },
    { 9.89, 243,   408,  651 },
    { 12.1, 393,   582,  975 },
    { 14.3, 578,   787,  1370 },
    { 16.6, 799,   1020, 1820 },
    { 19.0, 1080,  1310, 2390 },
    { 21.0, 1350,  1580, 2930 }
  };
  const double wlen = 0.6; /* Micron */
  double wlens[3] = { 0, 0, 0 };
  const double* dbls = NULL;
  size_t i, count;
  (void)argc, (void)argv;

  mem_init_proxy_allocator(&allocator, &mem_default_allocator);

  CHK(ssp_rng_create(&allocator, SSP_RNG_THREEFRY, &rng) == RES_OK);
  CHK(sschiff_device_create
    (NULL, &allocator, SSCHIFF_NTHREADS_DEFAULT, 1, NULL, &dev) == RES_OK);

  geometry_init_sphere(&sampler_ctx.geometry, 64);

  sampler_ctx.wavelength = wlen;
  sampler_ctx.relative_real_refractive_index = 1.1;
  sampler_ctx.relative_imaginary_refractive_index = 0.004;
  sampler_ctx.medium_refractive_index = 4.0/3.0;
  sampler_ctx.mean_radius = 1.0;
  sampler_ctx.sigma = 1.18;

  distrib.material.get_property = get_material_property;
  distrib.material.material = &sampler_ctx;
  distrib.characteristic_length = 1;
  distrib.sample = sample_sphere;
  distrib.context = &sampler_ctx;

  CHK(sschiff_integrate(NULL, NULL, NULL, NULL, 0, NULL, 1, 1, 1, 0, NULL) == RES_BAD_ARG);
  CHK(sschiff_integrate(dev, NULL, NULL, NULL, 0, NULL, 1, 1, 1, 0, NULL) == RES_BAD_ARG);
  CHK(sschiff_integrate(NULL, rng, NULL, NULL, 0, NULL, 1, 1, 1, 0, NULL) == RES_BAD_ARG);
  CHK(sschiff_integrate(dev, rng, NULL, NULL, 0, NULL, 1, 1, 1, 0, NULL) == RES_BAD_ARG);
  CHK(sschiff_integrate(NULL, NULL, &distrib, NULL, 0, NULL, 1, 1, 1, 0, NULL) == RES_BAD_ARG);
  CHK(sschiff_integrate(dev, NULL, &distrib, NULL, 0, NULL, 1, 1, 1, 0, NULL) == RES_BAD_ARG);
  CHK(sschiff_integrate(NULL, rng, &distrib, NULL, 0, NULL, 1, 1, 1, 0, NULL) == RES_BAD_ARG);
  CHK(sschiff_integrate(dev, rng, &distrib, NULL, 0, NULL, 1, 1, 1, 0, NULL) == RES_BAD_ARG);
  CHK(sschiff_integrate(NULL, NULL, NULL, &wlen, 0, NULL, 1, 1, 1, 0, NULL) == RES_BAD_ARG);
  CHK(sschiff_integrate(dev, NULL, NULL, &wlen, 0, NULL, 1, 1, 1, 0, NULL) == RES_BAD_ARG);
  CHK(sschiff_integrate(NULL, rng, NULL, &wlen, 0, NULL, 1, 1, 1, 0, NULL) == RES_BAD_ARG);
  CHK(sschiff_integrate(dev, rng, NULL, &wlen, 0, NULL, 1, 1, 1, 0, NULL) == RES_BAD_ARG);
  CHK(sschiff_integrate(NULL, NULL, &distrib, &wlen, 0, NULL, 1, 1, 1, 0, NULL) == RES_BAD_ARG);
  CHK(sschiff_integrate(dev, NULL, &distrib, &wlen, 0, NULL, 1, 1, 1, 0, NULL) == RES_BAD_ARG);
  CHK(sschiff_integrate(NULL, rng, &distrib, &wlen, 0, NULL, 1, 1, 1, 0, NULL) == RES_BAD_ARG);
  CHK(sschiff_integrate(dev, rng, &distrib, &wlen, 0, NULL, 1, 1, 1, 0, NULL) == RES_BAD_ARG);
  CHK(sschiff_integrate(NULL, NULL, NULL, NULL, 1, NULL, 1, 1, 1, 0, NULL) == RES_BAD_ARG);
  CHK(sschiff_integrate(dev, NULL, NULL, NULL, 1, NULL, 1, 1, 1, 0, NULL) == RES_BAD_ARG);
  CHK(sschiff_integrate(NULL, rng, NULL, NULL, 1, NULL, 1, 1, 1, 0, NULL) == RES_BAD_ARG);
  CHK(sschiff_integrate(dev, rng, NULL, NULL, 1, NULL, 1, 1, 1, 0, NULL) == RES_BAD_ARG);
  CHK(sschiff_integrate(NULL, NULL, &distrib, NULL, 1, NULL, 1, 1, 1, 0, NULL) == RES_BAD_ARG);
  CHK(sschiff_integrate(dev, NULL, &distrib, NULL, 1, NULL, 1, 1, 1, 0, NULL) == RES_BAD_ARG);
  CHK(sschiff_integrate(NULL, rng, &distrib, NULL, 1, NULL, 1, 1, 1, 0, NULL) == RES_BAD_ARG);
  CHK(sschiff_integrate(dev, rng, &distrib, NULL, 1, NULL, 1, 1, 1, 0, NULL) == RES_BAD_ARG);
  CHK(sschiff_integrate(NULL, NULL, NULL, &wlen, 1, NULL, 1, 1, 1, 0, NULL) == RES_BAD_ARG);
  CHK(sschiff_integrate(dev, NULL, NULL, &wlen, 1, NULL, 1, 1, 1, 0, NULL) == RES_BAD_ARG);
  CHK(sschiff_integrate(NULL, rng, NULL, &wlen, 1, NULL, 1, 1, 1, 0, NULL) == RES_BAD_ARG);
  CHK(sschiff_integrate(dev, rng, NULL, &wlen, 1, NULL, 1, 1, 1, 0, NULL) == RES_BAD_ARG);
  CHK(sschiff_integrate(NULL, NULL, &distrib, &wlen, 1, NULL, 1, 1, 1, 0, NULL) == RES_BAD_ARG);
  CHK(sschiff_integrate(dev, NULL, &distrib, &wlen, 1, NULL, 1, 1, 1, 0, NULL) == RES_BAD_ARG);
  CHK(sschiff_integrate(NULL, rng, &distrib, &wlen, 1, NULL, 1, 1, 1, 0, NULL) == RES_BAD_ARG);
  CHK(sschiff_integrate(dev, rng, &distrib, &wlen, 1, NULL, 1, 1, 1, 0, NULL) == RES_BAD_ARG);
  CHK(sschiff_integrate(NULL, NULL, NULL, NULL, 0, NULL, 1, 1, 1, 0, &estimator) == RES_BAD_ARG);
  CHK(sschiff_integrate(dev, NULL, NULL, NULL, 0, NULL, 1, 1, 1, 0, &estimator) == RES_BAD_ARG);
  CHK(sschiff_integrate(NULL, rng, NULL, NULL, 0, NULL, 1, 1, 1, 0, &estimator) == RES_BAD_ARG);
  CHK(sschiff_integrate(dev, rng, NULL, NULL, 0, NULL, 1, 1, 1, 0, &estimator) == RES_BAD_ARG);
  CHK(sschiff_integrate(NULL, NULL, &distrib, NULL, 0, NULL, 1, 1, 1, 0, &estimator) == RES_BAD_ARG);
  CHK(sschiff_integrate(dev, NULL, &distrib, NULL, 0, NULL, 1, 1, 1, 0, &estimator) == RES_BAD_ARG);
  CHK(sschiff_integrate(NULL, rng, &distrib, NULL, 0, NULL, 1, 1, 1, 0, &estimator) == RES_BAD_ARG);
  CHK(sschiff_integrate(dev, rng, &distrib, NULL, 0, NULL, 1, 1, 1, 0, &estimator) == RES_BAD_ARG);
  CHK(sschiff_integrate(NULL, NULL, NULL, &wlen, 0, NULL, 1, 1, 1, 0, &estimator) == RES_BAD_ARG);
  CHK(sschiff_integrate(dev, NULL, NULL, &wlen, 0, NULL, 1, 1, 1, 0, &estimator) == RES_BAD_ARG);
  CHK(sschiff_integrate(NULL, rng, NULL, &wlen, 0, NULL, 1, 1, 1, 0, &estimator) == RES_BAD_ARG);
  CHK(sschiff_integrate(dev, rng, NULL, &wlen, 0, NULL, 1, 1, 1, 0, &estimator) == RES_BAD_ARG);
  CHK(sschiff_integrate(NULL, NULL, &distrib, &wlen, 0, NULL, 1, 1, 1, 0, &estimator) == RES_BAD_ARG);
  CHK(sschiff_integrate(dev, NULL, &distrib, &wlen, 0, NULL, 1, 1, 1, 0, &estimator) == RES_BAD_ARG);
  CHK(sschiff_integrate(NULL, rng, &distrib, &wlen, 0, NULL, 1, 1, 1, 0, &estimator) == RES_BAD_ARG);
  CHK(sschiff_integrate(dev, rng, &distrib, &wlen, 0, NULL, 1, 1, 1, 0, &estimator) == RES_BAD_ARG);
  CHK(sschiff_integrate(NULL, NULL, NULL, NULL, 1, NULL, 1, 1, 1, 0, &estimator) == RES_BAD_ARG);
  CHK(sschiff_integrate(dev, NULL, NULL, NULL, 1, NULL, 1, 1, 1, 0, &estimator) == RES_BAD_ARG);
  CHK(sschiff_integrate(NULL, rng, NULL, NULL, 1, NULL, 1, 1, 1, 0, &estimator) == RES_BAD_ARG);
  CHK(sschiff_integrate(dev, rng, NULL, NULL, 1, NULL, 1, 1, 1, 0, &estimator) == RES_BAD_ARG);
  CHK(sschiff_integrate(NULL, NULL, &distrib, NULL, 1, NULL, 1, 1, 1, 0, &estimator) == RES_BAD_ARG);
  CHK(sschiff_integrate(dev, NULL, &distrib, NULL, 1, NULL, 1, 1, 1, 0, &estimator) == RES_BAD_ARG);
  CHK(sschiff_integrate(NULL, rng, &distrib, NULL, 1, NULL, 1, 1, 1, 0, &estimator) == RES_BAD_ARG);
  CHK(sschiff_integrate(dev, rng, &distrib, NULL, 1, NULL, 1, 1, 1, 0, &estimator) == RES_BAD_ARG);
  CHK(sschiff_integrate(NULL, NULL, NULL, &wlen, 1, NULL, 1, 1, 1, 0, &estimator) == RES_BAD_ARG);
  CHK(sschiff_integrate(dev, NULL, NULL, &wlen, 1, NULL, 1, 1, 1, 0, &estimator) == RES_BAD_ARG);
  CHK(sschiff_integrate(NULL, rng, NULL, &wlen, 1, NULL, 1, 1, 1, 0, &estimator) == RES_BAD_ARG);
  CHK(sschiff_integrate(dev, rng, NULL, &wlen, 1, NULL, 1, 1, 1, 0, &estimator) == RES_BAD_ARG);
  CHK(sschiff_integrate(NULL, NULL, &distrib, &wlen, 1, NULL, 1, 1, 1, 0, &estimator) == RES_BAD_ARG);
  CHK(sschiff_integrate(dev, NULL, &distrib, &wlen, 1, NULL, 1, 1, 1, 0, &estimator) == RES_BAD_ARG);
  CHK(sschiff_integrate(NULL, rng, &distrib, &wlen, 1, NULL, 1, 1, 1, 0, &estimator) == RES_BAD_ARG);
  CHK(sschiff_integrate(dev, rng, &distrib, &wlen, 1, NULL, 1, 1, 1, 0, &estimator) == RES_BAD_ARG);

  CHK(sschiff_integrate(dev, rng, &distrib, &wlen, 1,
    sschiff_uniform_scattering_angles, 1, 1, 1, 0, &estimator) == RES_BAD_ARG);
  CHK(sschiff_integrate(dev, rng, &distrib, &wlen, 1,
    sschiff_uniform_scattering_angles, 10, 1000, 1, 0, &estimator) == RES_OK);

  CHK(sschiff_estimator_get_wavelengths(NULL, NULL, NULL) == RES_BAD_ARG);
  CHK(sschiff_estimator_get_wavelengths(estimator, NULL, NULL) == RES_OK);/*Useless*/
  CHK(sschiff_estimator_get_wavelengths(NULL, NULL, &count) == RES_BAD_ARG);
  CHK(sschiff_estimator_get_wavelengths(estimator, NULL, &count) == RES_OK);
  CHK(count == 1);
  CHK(sschiff_estimator_get_wavelengths(NULL, &dbls, NULL) == RES_BAD_ARG);
  CHK(sschiff_estimator_get_wavelengths(estimator, &dbls, NULL) == RES_OK);
  CHK(dbls[0] == wlen);
  CHK(sschiff_estimator_get_wavelengths(NULL, &dbls, &count) == RES_BAD_ARG);
  CHK(sschiff_estimator_get_wavelengths(estimator, &dbls, &count) == RES_OK);
  CHK(dbls[0] == wlen);
  CHK(count == 1);

  CHK(sschiff_estimator_get_scattering_angles(NULL, NULL, NULL) == RES_BAD_ARG);
  CHK(sschiff_estimator_get_scattering_angles(estimator, NULL, NULL) == RES_OK); /* Useless */
  CHK(sschiff_estimator_get_scattering_angles(NULL, NULL, &count) == RES_BAD_ARG);
  CHK(sschiff_estimator_get_scattering_angles(estimator, NULL, &count) == RES_OK);
  CHK(count == 10);
  CHK(sschiff_estimator_get_scattering_angles(NULL, &dbls, NULL) == RES_BAD_ARG);
  CHK(sschiff_estimator_get_scattering_angles(estimator, &dbls, NULL) == RES_OK); /* Useless */
  FOR_EACH(i, 0, count)
    CHK(eq_eps(dbls[i], (double)i*PI/((double)count-1), 1.e-6) == 1);
  CHK(sschiff_estimator_get_scattering_angles(NULL, &dbls, &count) == RES_BAD_ARG);
  CHK(sschiff_estimator_get_scattering_angles(estimator, &dbls, &count) == RES_OK);
  FOR_EACH(i, 0, count)
    CHK(eq_eps(dbls[i], (double)i*PI/((double)count-1), 1.e-6) == 1);
  CHK(count == 10);

  CHK(sschiff_estimator_get_realisations_count(NULL, NULL) == RES_BAD_ARG);
  CHK(sschiff_estimator_get_realisations_count(estimator, NULL) == RES_BAD_ARG);
  CHK(sschiff_estimator_get_realisations_count(NULL, &count) == RES_BAD_ARG);
  CHK(sschiff_estimator_get_realisations_count(estimator, &count) == RES_OK);
  CHK(count == 1000);

  CHK(sschiff_estimator_get_cross_section(NULL, 1, NULL) == RES_BAD_ARG);
  CHK(sschiff_estimator_get_cross_section(estimator, 1, NULL) == RES_BAD_ARG);
  CHK(sschiff_estimator_get_cross_section(NULL, 1, &cross_section) == RES_BAD_ARG);
  CHK(sschiff_estimator_get_cross_section(estimator, 1, &cross_section) == RES_BAD_ARG);
  CHK(sschiff_estimator_get_cross_section(NULL, 0, NULL) == RES_BAD_ARG);
  CHK(sschiff_estimator_get_cross_section(estimator, 0, NULL) == RES_BAD_ARG);
  CHK(sschiff_estimator_get_cross_section(NULL, 0, &cross_section) == RES_BAD_ARG);
  CHK(sschiff_estimator_get_cross_section(estimator, 0, &cross_section) == RES_OK);

  CHK(sschiff_estimator_ref_get(NULL) == RES_BAD_ARG);
  CHK(sschiff_estimator_ref_get(estimator) == RES_OK);
  CHK(sschiff_estimator_ref_put(NULL) == RES_BAD_ARG);
  CHK(sschiff_estimator_ref_put(estimator) == RES_OK);
  CHK(sschiff_estimator_ref_put(estimator) == RES_OK);

  wlens[0] = 0.2, wlens[1] = 0.6, wlens[2] = 0.4;
  CHK(sschiff_integrate(dev, rng, &distrib, wlens, 3,
    sschiff_uniform_scattering_angles, 10, 1000, 1, 0, &estimator) == RES_BAD_ARG);

  CHK(sschiff_integrate(dev, rng, &distrib, &wlen, 1,
    sschiff_uniform_scattering_angles, 2, 1, 1, 1, &estimator) == RES_OK);
  CHK(sschiff_estimator_ref_put(estimator) == RES_OK);

  sampler_ctx.relative_real_refractive_index = 1.1;
  check_schiff_estimation(dev, rng, &sampler_ctx, results_n_r_1_1,
    sizeof(results_n_r_1_1)/sizeof(struct result));

  printf("\n");
  sampler_ctx.relative_real_refractive_index = 1.01;
  check_schiff_estimation(dev, rng, &sampler_ctx, results_n_r_1_01,
    sizeof(results_n_r_1_01)/sizeof(struct result));

  CHK(sschiff_device_ref_put(dev) == RES_OK);
  CHK(ssp_rng_ref_put(rng) == RES_OK);

  geometry_release(&sampler_ctx.geometry);
  check_memory_allocator(&allocator);
  mem_shutdown_proxy_allocator(&allocator);
  CHK(mem_allocated_size() == 0);
  return 0;
}

