/* Copyright (C) 2020, 2021 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2015, 2016 CNRS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "sschiff.h"
#include "test_sschiff_utils.h"

#include <rsys/logger.h>
#include <rsys/mem_allocator.h>
#include <rsys/rsys.h>

#include <star/s3d.h>
#define N SSCHIFF_NTHREADS_DEFAULT

static void
log_stream(const char* msg, void* ctx)
{
  ASSERT(msg);
  (void)msg, (void)ctx;
  printf("%s\n", msg);
}

int
main(int argc, char** argv)
{
  struct logger logger;
  struct mem_allocator allocator;
  struct sschiff_device* dev;
  struct s3d_device* s3d;
  unsigned nthreads;

  (void)argc, (void)argv;

  CHK(sschiff_device_create(NULL, NULL, N, 0, NULL, NULL) == RES_BAD_ARG);
  CHK(sschiff_device_create(NULL, NULL, N, 0, NULL, &dev) == RES_OK);

  CHK(sschiff_device_ref_get(NULL) == RES_BAD_ARG);
  CHK(sschiff_device_ref_get(dev) == RES_OK);
  CHK(sschiff_device_ref_put(NULL) == RES_BAD_ARG);
  CHK(sschiff_device_ref_put(dev) == RES_OK);
  CHK(sschiff_device_ref_put(dev) == RES_OK);

  mem_init_proxy_allocator(&allocator, &mem_default_allocator);

  CHK(MEM_ALLOCATED_SIZE(&allocator) == 0);
  CHK(sschiff_device_create(NULL, &allocator, N, 1, NULL, NULL) == RES_BAD_ARG);
  CHK(sschiff_device_create(NULL, &allocator, N, 1, NULL, &dev) == RES_OK);
  CHK(sschiff_device_ref_put(dev) == RES_OK);
  CHK(MEM_ALLOCATED_SIZE(&allocator) == 0);

  CHK(logger_init(&allocator, &logger) == RES_OK);
  logger_set_stream(&logger, LOG_OUTPUT, log_stream, NULL);
  logger_set_stream(&logger, LOG_ERROR, log_stream, NULL);
  logger_set_stream(&logger, LOG_WARNING, log_stream, NULL);

  CHK(sschiff_device_create(&logger, NULL, N, 0, NULL, NULL) == RES_BAD_ARG);
  CHK(sschiff_device_create(&logger, NULL, N, 0, NULL, &dev) == RES_OK);
  CHK(sschiff_device_ref_put(dev) == RES_OK);

  CHK(sschiff_device_create(&logger, &allocator, N, 1, NULL, NULL) == RES_BAD_ARG);
  CHK(sschiff_device_create(&logger, &allocator, N, 1, NULL, &dev) == RES_OK);
  CHK(sschiff_device_ref_put(dev) == RES_OK);

  CHK(s3d_device_create(NULL, NULL, 0, &s3d) == RES_OK);
  CHK(sschiff_device_create(NULL, NULL, N, 0, s3d, NULL) == RES_BAD_ARG);
  CHK(sschiff_device_create(NULL, NULL, N, 0, s3d, &dev) == RES_OK);

  CHK(s3d_device_ref_put(s3d) == RES_OK);
  CHK(sschiff_device_ref_put(dev) == RES_OK);

  CHK(sschiff_device_create(NULL, NULL, 0, 0, NULL, &dev) == RES_BAD_ARG);
  CHK(sschiff_device_create(NULL, NULL, 1, 0, NULL, &dev) == RES_OK);

  CHK(sschiff_device_get_threads_count(NULL, NULL) == RES_BAD_ARG);
  CHK(sschiff_device_get_threads_count(dev, NULL) == RES_BAD_ARG);
  CHK(sschiff_device_get_threads_count(NULL, &nthreads) == RES_BAD_ARG);
  CHK(sschiff_device_get_threads_count(dev, &nthreads) == RES_OK);
  CHK(nthreads == 1);

  CHK(sschiff_device_ref_put(dev) == RES_OK);

  logger_release(&logger);
  check_memory_allocator(&allocator);
  mem_shutdown_proxy_allocator(&allocator);
  CHK(mem_allocated_size() == 0);
  return 0;
}

