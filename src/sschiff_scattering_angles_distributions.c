/* Copyright (C) 2020, 2021 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2015, 2016 CNRS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "sschiff.h"
#include <rsys/math.h>

static void
uniform(double* angles, const size_t nangles)
{
  size_t iangle;
  const double step = PI / (double)(nangles - 1);
  ASSERT(nangles >= 2);

  angles[0] = 0.0;
  FOR_EACH(iangle, 1, nangles-1) {
    angles[iangle] = angles[iangle-1] + step;
  }
  angles[nangles-1] = PI;
}

const sschiff_scattering_angles_distribution_T 
sschiff_uniform_scattering_angles = uniform;

