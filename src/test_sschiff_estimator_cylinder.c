/* Copyright (C) 2020, 2021 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2015, 2016 CNRS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "sschiff.h"
#include "test_sschiff_utils.h"

#include <rsys/clock_time.h>
#include <rsys/float3.h>
#include <rsys/stretchy_array.h>

#include <star/s3d.h>
#include <star/ssp.h>

struct result {
  double absorption_E;
  double absorption_SE;
  double scattering_E;
  double scattering_SE;
  double extinction_E;
  double extinction_SE;
  double avg_proj_area_E;
  double avg_proj_area_SE;
};

struct sampler_context {
  struct s3d_shape* shape;
  double cylinder_volume;
  double mean_radius;
  double sigma;
};

static void
get_material_property
  (void* mtl,
   const double wavelength,
   struct sschiff_material_properties* props)
{
  (void)mtl;
  CHK(eq_eps(wavelength, 0.6, 1.e-6) == 1);
  props->medium_refractive_index = 4.0/3.0;
  props->relative_imaginary_refractive_index = 4.e-3;
  props->relative_real_refractive_index = 1.01;
}

static res_T
sample_cylinder
  (struct ssp_rng* rng,
   void** shape_data,
   struct s3d_shape* shape,
   void* sampler_context)
{
  struct sampler_context* sampler_ctx = sampler_context;
  ASSERT(sampler_context && shape_data);
  (void)rng;
  *shape_data = (void*)((intptr_t)0xDECAFBAD);
  return s3d_mesh_copy(sampler_ctx->shape, shape);
}

static res_T
sample_volume_scaling
  (struct ssp_rng* rng,
   void* shape,
   double* volume_scaling,
   void* sampler_context)
{
  struct sampler_context* sampler_ctx = sampler_context;
  double sample;
  double sphere_volume;
  ASSERT(sampler_context);
 
  CHK(shape == (void*)((intptr_t)0xDECAFBAD));

  sample = ssp_ran_lognormal(rng, log(sampler_ctx->mean_radius), log(sampler_ctx->sigma));
  sphere_volume = 4.0*PI*sample*sample*sample / 3.0;
  *volume_scaling = sphere_volume / sampler_ctx->cylinder_volume;
  return RES_OK;
}

static INLINE void
compute_intersection
  (double intersection[2], double E0, double SE0, double E1, double SE1)
{
  double interval0[2], interval1[2];
  interval0[0] = E0 - 4*SE0;
  interval0[1] = E0 + 4*SE0;
  interval1[0] = E1 - 4*SE1;
  interval1[1] = E1 + 4*SE1;
  intersection[0] = MMAX(interval0[0], interval1[0]);
  intersection[1] = MMIN(interval0[1], interval1[1]);
}

int
main(int argc, char** argv)
{
  char buf[64];
  struct mem_allocator allocator;
  struct sampler_context sampler_ctx;
  struct sschiff_device* dev;
  struct sschiff_geometry_distribution distrib = SSCHIFF_NULL_GEOMETRY_DISTRIBUTION;
  struct sschiff_estimator* estimator;
  struct ssp_rng* rng;
  struct time t0, t1;
  const struct result results[] = {
    { 5.29715E-19, 4.28609E-22, 1.71856E-25, 2.19472E-28, 5.29715E-19, 4.28609E-22, 3.81205E-12, 4.22555E-16 },
    { 1.13812E-04, 9.18322E-08, 2.21384E-06, 2.82025E-09, 1.16026E-04, 9.45526E-08, 1.37239E-02, 1.52125E-06 },
    { 9.05619E-04, 7.28692E-07, 3.52032E-05, 4.47348E-08, 9.40822E-04, 7.71882E-07, 5.48945E-02, 6.08489E-06 },
    { 3.04018E-03, 2.43947E-06, 1.77116E-04, 2.24508E-07, 3.21730E-03, 2.65641E-06, 1.23512E-01, 1.36909E-05 },
    { 7.16810E-03, 5.73592E-06, 5.56304E-04, 7.03373E-07, 7.72440E-03, 6.41615E-06, 2.19576E-01, 2.43394E-05 },
    { 1.39261E-02, 1.11131E-05, 1.34971E-03, 1.70216E-06, 1.52758E-02, 1.27606E-05, 3.43087E-01, 3.80302E-05 },
    { 2.39373E-02, 1.90499E-05, 2.78123E-03, 3.49844E-06, 2.67186E-02, 2.24385E-05, 4.94045E-01, 5.47634E-05 },
    { 3.78116E-02, 3.00096E-05, 5.12017E-03, 6.42370E-06, 4.29318E-02, 3.62361E-05, 6.72450E-01, 7.45390E-05 },
    { 5.61457E-02, 4.44398E-05, 8.67959E-03, 1.08606E-05, 6.48252E-02, 5.49743E-05, 8.78301E-01, 9.73571E-05 },
    { 7.95235E-02, 6.27738E-05, 1.38147E-02, 1.72400E-05, 9.33382E-02, 7.95073E-05, 1.11160E+00, 1.23217E-04 },
    { 1.08517E-01, 8.54299E-05, 2.09215E-02, 2.60386E-05, 1.29438E-01, 1.10720E-04, 1.37234E+00, 1.52120E-04 },
    { 1.43684E-01, 1.12813E-04, 3.04347E-02, 3.77758E-05, 1.74119E-01, 1.49525E-04, 1.66054E+00, 1.84065E-04 },
    { 1.85573E-01, 1.45313E-04, 4.28269E-02, 5.30113E-05, 2.28400E-01, 1.96862E-04, 1.97617E+00, 2.19053E-04 },
    { 2.34719E-01, 1.83309E-04, 5.86064E-02, 7.23425E-05, 2.93325E-01, 2.53696E-04, 2.31926E+00, 2.57083E-04 },
    { 2.91645E-01, 2.27165E-04, 7.83160E-02, 9.64016E-05, 3.69961E-01, 3.21011E-04, 2.68979E+00, 2.98155E-04 },
    { 3.56865E-01, 2.77233E-04, 1.02531E-01, 1.25854E-04, 4.59396E-01, 3.99816E-04, 3.08777E+00, 3.42270E-04 },
    { 4.30879E-01, 3.33853E-04, 1.31860E-01, 1.61393E-04, 5.62739E-01, 4.91132E-04, 3.51320E+00, 3.89427E-04 },
    { 5.14178E-01, 3.97355E-04, 1.66938E-01, 2.03742E-04, 6.81116E-01, 5.96001E-04, 3.96607E+00, 4.39627E-04 },
    { 6.07243E-01, 4.68053E-04, 2.08431E-01, 2.53648E-04, 8.15674E-01, 7.15476E-04, 4.44639E+00, 4.92869E-04 },
    { 7.10542E-01, 5.46255E-04, 2.57033E-01, 3.11882E-04, 9.67574E-01, 8.50622E-04, 4.95415E+00, 5.49153E-04 },
    { 8.24534E-01, 6.32256E-04, 3.13460E-01, 3.79232E-04, 1.13799E+00, 1.00252E-03, 5.48937E+00, 6.08480E-04 },
    { 9.49670E-01, 7.26339E-04, 3.78455E-01, 4.56509E-04, 1.32813E+00, 1.17224E-03, 6.05203E+00, 6.70849E-04 },
    { 1.08639E+00, 8.28779E-04, 4.52783E-01, 5.44535E-04, 1.53917E+00, 1.36088E-03, 6.64213E+00, 7.36261E-04 },
    { 1.23512E+00, 9.39839E-04, 5.37230E-01, 6.44150E-04, 1.77235E+00, 1.56954E-03, 7.25969E+00, 8.04715E-04 },
    { 1.39628E+00, 1.05978E-03, 6.32603E-01, 7.56203E-04, 2.02888E+00, 1.79931E-03, 7.90469E+00, 8.76211E-04 },
    { 1.57029E+00, 1.18883E-03, 7.39725E-01, 8.81552E-04, 2.31001E+00, 2.05129E-03, 8.57713E+00, 9.50750E-04 },
    { 1.75754E+00, 1.32725E-03, 8.59440E-01, 1.02107E-03, 2.61698E+00, 2.32657E-03, 9.27703E+00, 1.02833E-03 },
    { 1.95844E+00, 1.47525E-03, 9.92605E-01, 1.17562E-03, 2.95104E+00, 2.62624E-03, 1.00044E+01, 1.10895E-03 },
    { 2.17335E+00, 1.63305E-03, 1.14009E+00, 1.34608E-03, 3.31344E+00, 2.95141E-03, 1.07592E+01, 1.19262E-03 },
    { 2.40267E+00, 1.80087E-03, 1.30279E+00, 1.53333E-03, 3.70546E+00, 3.30314E-03, 1.15414E+01, 1.27933E-03 },
    { 2.64676E+00, 1.97891E-03, 1.48159E+00, 1.73825E-03, 4.12835E+00, 3.68251E-03, 1.23511E+01, 1.36908E-03 },
    { 2.90597E+00, 2.16735E-03, 1.67741E+00, 1.96171E-03, 4.58338E+00, 4.09060E-03, 1.31882E+01, 1.46187E-03 },
    { 3.18066E+00, 2.36640E-03, 1.89117E+00, 2.20460E-03, 5.07183E+00, 4.52845E-03, 1.40528E+01, 1.55771E-03 },
    { 3.47118E+00, 2.57622E-03, 2.12379E+00, 2.46776E-03, 5.59497E+00, 4.99711E-03, 1.49448E+01, 1.65659E-03 },
    { 3.77786E+00, 2.79699E-03, 2.37621E+00, 2.75208E-03, 6.15407E+00, 5.49762E-03, 1.58643E+01, 1.75851E-03 },
    { 4.10102E+00, 3.02889E-03, 2.64937E+00, 3.05839E-03, 6.75039E+00, 6.03099E-03, 1.68112E+01, 1.86347E-03 },
    { 4.44100E+00, 3.27205E-03, 2.94421E+00, 3.38755E-03, 7.38521E+00, 6.59822E-03, 1.77855E+01, 1.97147E-03 },
    { 4.79810E+00, 3.52665E-03, 3.26169E+00, 3.74039E-03, 8.05979E+00, 7.20030E-03, 1.87873E+01, 2.08252E-03 },
    { 5.17263E+00, 3.79282E-03, 3.60277E+00, 4.11772E-03, 8.77539E+00, 7.83819E-03, 1.98166E+01, 2.19661E-03 },
    { 5.56489E+00, 4.07070E-03, 3.96838E+00, 4.52037E-03, 9.53327E+00, 8.51284E-03, 2.08733E+01, 2.31374E-03 },
    { 5.97518E+00, 4.36043E-03, 4.35950E+00, 4.94911E-03, 1.03347E+01, 9.22518E-03, 2.19575E+01, 2.43392E-03 },
    { 6.40378E+00, 4.66214E-03, 4.77708E+00, 5.40474E-03, 1.11809E+01, 9.97612E-03, 2.30690E+01, 2.55714E-03 },
    { 6.85097E+00, 4.97595E-03, 5.22207E+00, 5.88800E-03, 1.20730E+01, 1.07665E-02, 2.42081E+01, 2.68339E-03 },
    { 7.31703E+00, 5.30197E-03, 5.69542E+00, 6.39965E-03, 1.30125E+01, 1.15973E-02, 2.53746E+01, 2.81270E-03 },
    { 7.80222E+00, 5.64032E-03, 6.19810E+00, 6.94041E-03, 1.40003E+01, 1.24693E-02, 2.65685E+01, 2.94504E-03 },
    { 8.30681E+00, 5.99110E-03, 6.73103E+00, 7.51098E-03, 1.50378E+01, 1.33832E-02, 2.77899E+01, 3.08043E-03 },
    { 8.83105E+00, 6.35442E-03, 7.29517E+00, 8.11205E-03, 1.61262E+01, 1.43399E-02, 2.90387E+01, 3.21886E-03 },
    { 9.37519E+00, 6.73037E-03, 7.89146E+00, 8.74428E-03, 1.72666E+01, 1.53402E-02, 3.03150E+01, 3.36033E-03 },
    { 9.93948E+00, 7.11905E-03, 8.52081E+00, 9.40831E-03, 1.84603E+01, 1.63848E-02, 3.16187E+01, 3.50484E-03 },
    { 1.05241E+01, 7.52055E-03, 9.18416E+00, 1.01048E-02, 1.97083E+01, 1.74744E-02, 3.29499E+01, 3.65240E-03 },
    { 1.11294E+01, 7.93493E-03, 9.88241E+00, 1.08342E-02, 2.10118E+01, 1.86096E-02, 3.43085E+01, 3.80300E-03 },
    { 1.17556E+01, 8.36230E-03, 1.06165E+01, 1.15973E-02, 2.23720E+01, 1.97912E-02, 3.56946E+01, 3.95664E-03 },
    { 1.24028E+01, 8.80272E-03, 1.13873E+01, 1.23945E-02, 2.37900E+01, 2.10198E-02, 3.71081E+01, 4.11332E-03 },
    { 1.30712E+01, 9.25625E-03, 1.21956E+01, 1.32263E-02, 2.52669E+01, 2.22959E-02, 3.85490E+01, 4.27305E-03 },
    { 1.37612E+01, 9.72298E-03, 1.30425E+01, 1.40933E-02, 2.68037E+01, 2.36201E-02, 4.00174E+01, 4.43582E-03 },
    { 1.44729E+01, 1.02030E-02, 1.39287E+01, 1.49959E-02, 2.84016E+01, 2.49931E-02, 4.15133E+01, 4.60163E-03 },
    { 1.52065E+01, 1.06963E-02, 1.48550E+01, 1.59346E-02, 3.00616E+01, 2.64152E-02, 4.30366E+01, 4.77048E-03 },
    { 1.59622E+01, 1.12029E-02, 1.58225E+01, 1.69097E-02, 3.17846E+01, 2.78869E-02, 4.45873E+01, 4.94237E-03 },
    { 1.67402E+01, 1.17230E-02, 1.68317E+01, 1.79217E-02, 3.35719E+01, 2.94088E-02, 4.61655E+01, 5.11731E-03 },
    { 1.75407E+01, 1.22566E-02, 1.78836E+01, 1.89710E-02, 3.54243E+01, 3.09811E-02, 4.77712E+01, 5.29529E-03 },
    { 1.83638E+01, 1.28036E-02, 1.89790E+01, 2.00578E-02, 3.73428E+01, 3.26045E-02, 4.94043E+01, 5.47631E-03 },
    { 1.92098E+01, 1.33643E-02, 2.01186E+01, 2.11824E-02, 3.93284E+01, 3.42790E-02, 5.10648E+01, 5.66038E-03 },
    { 2.00788E+01, 1.39385E-02, 2.13033E+01, 2.23453E-02, 4.13821E+01, 3.60052E-02, 5.27528E+01, 5.84749E-03 },
    { 2.09710E+01, 1.45264E-02, 2.25337E+01, 2.35465E-02, 4.35047E+01, 3.77833E-02, 5.44682E+01, 6.03764E-03 },
    { 2.18866E+01, 1.51280E-02, 2.38106E+01, 2.47864E-02, 4.56972E+01, 3.96136E-02, 5.62111E+01, 6.23083E-03 },
    { 2.28257E+01, 1.57432E-02, 2.51347E+01, 2.60651E-02, 4.79604E+01, 4.14962E-02, 5.79814E+01, 6.42706E-03 },
    { 2.37885E+01, 1.63722E-02, 2.65067E+01, 2.73829E-02, 5.02952E+01, 4.34315E-02, 5.97791E+01, 6.62634E-03 },
    { 2.47751E+01, 1.70150E-02, 2.79274E+01, 2.87397E-02, 5.27025E+01, 4.54196E-02, 6.16044E+01, 6.82866E-03 },
    { 2.57857E+01, 1.76715E-02, 2.93973E+01, 3.01359E-02, 5.51830E+01, 4.74606E-02, 6.34570E+01, 7.03402E-03 },
    { 2.68205E+01, 1.83419E-02, 3.09171E+01, 3.15714E-02, 5.77375E+01, 4.95546E-02, 6.53371E+01, 7.24243E-03 },
    { 2.78795E+01, 1.90261E-02, 3.24874E+01, 3.30463E-02, 6.03669E+01, 5.17018E-02, 6.72447E+01, 7.45387E-03 },
    { 2.89630E+01, 1.97242E-02, 3.41089E+01, 3.45606E-02, 6.30719E+01, 5.39023E-02, 6.91797E+01, 7.66836E-03 },
    { 3.00710E+01, 2.04361E-02, 3.57822E+01, 3.61145E-02, 6.58531E+01, 5.61559E-02, 7.11421E+01, 7.88589E-03 },
    { 3.12037E+01, 2.11619E-02, 3.75077E+01, 3.77077E-02, 6.87114E+01, 5.84628E-02, 7.31320E+01, 8.10647E-03 },
    { 3.23612E+01, 2.19015E-02, 3.92861E+01, 3.93404E-02, 7.16474E+01, 6.08230E-02, 7.51494E+01, 8.33008E-03 },
    { 3.35437E+01, 2.26551E-02, 4.11179E+01, 4.10124E-02, 7.46617E+01, 6.32362E-02, 7.71941E+01, 8.55674E-03 },
    { 3.47513E+01, 2.34226E-02, 4.30036E+01, 4.27236E-02, 7.77550E+01, 6.57026E-02, 7.92664E+01, 8.78644E-03 },
    { 3.59841E+01, 2.42040E-02, 4.49438E+01, 4.44740E-02, 8.09278E+01, 6.82220E-02, 8.13661E+01, 9.01918E-03 },
    { 3.72422E+01, 2.49994E-02, 4.69387E+01, 4.62633E-02, 8.41809E+01, 7.07942E-02, 8.34932E+01, 9.25497E-03 },
    { 3.85257E+01, 2.58086E-02, 4.89890E+01, 4.80915E-02, 8.75147E+01, 7.34190E-02, 8.56478E+01, 9.49380E-03 },
    { 3.98347E+01, 2.66318E-02, 5.10951E+01, 4.99583E-02, 9.09298E+01, 7.60964E-02, 8.78298E+01, 9.73567E-03 },
    { 4.11695E+01, 2.74689E-02, 5.32573E+01, 5.18635E-02, 9.44268E+01, 7.88261E-02, 9.00392E+01, 9.98058E-03 },
    { 4.25299E+01, 2.83199E-02, 5.54761E+01, 5.38068E-02, 9.80060E+01, 8.16078E-02, 9.22762E+01, 1.02285E-02 },
    { 4.39163E+01, 2.91849E-02, 5.77517E+01, 5.57881E-02, 1.01668E+02, 8.44413E-02, 9.45405E+01, 1.04795E-02 },
    { 4.53286E+01, 3.00637E-02, 6.00847E+01, 5.78070E-02, 1.05413E+02, 8.73264E-02, 9.68323E+01, 1.07336E-02 },
    { 4.67669E+01, 3.09565E-02, 6.24752E+01, 5.98633E-02, 1.09242E+02, 9.02627E-02, 9.91516E+01, 1.09907E-02 },
    { 4.82314E+01, 3.18632E-02, 6.49236E+01, 6.19566E-02, 1.13155E+02, 9.32498E-02, 1.01498E+02, 1.12508E-02 },
    { 4.97222E+01, 3.27838E-02, 6.74302E+01, 6.40865E-02, 1.17152E+02, 9.62875E-02, 1.03872E+02, 1.15139E-02 },
    { 5.12393E+01, 3.37182E-02, 6.99952E+01, 6.62528E-02, 1.21235E+02, 9.93754E-02, 1.06274E+02, 1.17802E-02 },
    { 5.27829E+01, 3.46665E-02, 7.26189E+01, 6.84550E-02, 1.25402E+02, 1.02513E-01, 1.08703E+02, 1.20494E-02 },
    { 5.43529E+01, 3.56287E-02, 7.53015E+01, 7.06928E-02, 1.29654E+02, 1.05700E-01, 1.11160E+02, 1.23217E-02 },
    { 5.59496E+01, 3.66048E-02, 7.80432E+01, 7.29657E-02, 1.33993E+02, 1.08936E-01, 1.13643E+02, 1.25970E-02 },
    { 5.75729E+01, 3.75947E-02, 8.08442E+01, 7.52733E-02, 1.38417E+02, 1.12220E-01, 1.16155E+02, 1.28754E-02 },
    { 5.92230E+01, 3.85984E-02, 8.37046E+01, 7.76152E-02, 1.42928E+02, 1.15553E-01, 1.18694E+02, 1.31568E-02 },
    { 6.09000E+01, 3.96159E-02, 8.66246E+01, 7.99908E-02, 1.47525E+02, 1.18933E-01, 1.21260E+02, 1.34413E-02 },
    { 6.26039E+01, 4.06472E-02, 8.96044E+01, 8.23998E-02, 1.52208E+02, 1.22360E-01, 1.23854E+02, 1.37288E-02 },
    { 6.43347E+01, 4.16923E-02, 9.26439E+01, 8.48416E-02, 1.56979E+02, 1.25834E-01, 1.26475E+02, 1.40194E-02 },
    { 6.60926E+01, 4.27512E-02, 9.57432E+01, 8.73157E-02, 1.61836E+02, 1.29353E-01, 1.29123E+02, 1.43130E-02 },
    { 6.78777E+01, 4.38238E-02, 9.89026E+01, 8.98216E-02, 1.66780E+02, 1.32919E-01, 1.31800E+02, 1.46096E-02 },
    { 6.96899E+01, 4.49101E-02, 1.02122E+02, 9.23587E-02, 1.71812E+02, 1.36529E-01, 1.34503E+02, 1.49093E-02 },
    { 7.15294E+01, 4.60101E-02, 1.05401E+02, 9.49266E-02, 1.76931E+02, 1.40183E-01, 1.37234E+02, 1.52120E-02 },
    { 7.33962E+01, 4.71238E-02, 1.08740E+02, 9.75247E-02, 1.82137E+02, 1.43881E-01, 1.39992E+02, 1.55177E-02 },
    { 7.52904E+01, 4.82512E-02, 1.12140E+02, 1.00152E-01, 1.87430E+02, 1.47622E-01, 1.42778E+02, 1.58265E-02 },
    { 7.72121E+01, 4.93922E-02, 1.15599E+02, 1.02809E-01, 1.92811E+02, 1.51406E-01, 1.45592E+02, 1.61384E-02 },
    { 7.91612E+01, 5.05468E-02, 1.19118E+02, 1.05494E-01, 1.98279E+02, 1.55232E-01, 1.48432E+02, 1.64533E-02 },
    { 8.11380E+01, 5.17150E-02, 1.22697E+02, 1.08207E-01, 2.03835E+02, 1.59099E-01, 1.51300E+02, 1.67712E-02 },
    { 8.31423E+01, 5.28967E-02, 1.26335E+02, 1.10947E-01, 2.09478E+02, 1.63007E-01, 1.54196E+02, 1.70922E-02 },
    { 8.51743E+01, 5.40921E-02, 1.30033E+02, 1.13714E-01, 2.15208E+02, 1.66954E-01, 1.57119E+02, 1.74162E-02 },
    { 8.72341E+01, 5.53009E-02, 1.33791E+02, 1.16507E-01, 2.21025E+02, 1.70941E-01, 1.60070E+02, 1.77433E-02 },
    { 8.93216E+01, 5.65232E-02, 1.37608E+02, 1.19325E-01, 2.26930E+02, 1.74967E-01, 1.63048E+02, 1.80734E-02 },
    { 9.14369E+01, 5.77590E-02, 1.41484E+02, 1.22168E-01, 2.32921E+02, 1.79031E-01, 1.66053E+02, 1.84065E-02 },
    { 9.35801E+01, 5.90083E-02, 1.45419E+02, 1.25036E-01, 2.38999E+02, 1.83132E-01, 1.69086E+02, 1.87427E-02 },
    { 9.57512E+01, 6.02710E-02, 1.49413E+02, 1.27926E-01, 2.45164E+02, 1.87269E-01, 1.72146E+02, 1.90819E-02 },
    { 9.79503E+01, 6.15471E-02, 1.53465E+02, 1.30840E-01, 2.51416E+02, 1.91443E-01, 1.75234E+02, 1.94242E-02 },
    { 1.00177E+02, 6.28365E-02, 1.57576E+02, 1.33776E-01, 2.57754E+02, 1.95652E-01, 1.78349E+02, 1.97695E-02 },
    { 1.02432E+02, 6.41393E-02, 1.61745E+02, 1.36733E-01, 2.64178E+02, 1.99896E-01, 1.81492E+02, 2.01178E-02 },
    { 1.04716E+02, 6.54554E-02, 1.65972E+02, 1.39711E-01, 2.70687E+02, 2.04173E-01, 1.84662E+02, 2.04692E-02 },
    { 1.07027E+02, 6.67848E-02, 1.70256E+02, 1.42710E-01, 2.77283E+02, 2.08484E-01, 1.87860E+02, 2.08237E-02 },
    { 1.09366E+02, 6.81274E-02, 1.74597E+02, 1.45728E-01, 2.83964E+02, 2.12827E-01, 1.91085E+02, 2.11812E-02 },
    { 1.11734E+02, 6.94833E-02, 1.78996E+02, 1.48765E-01, 2.90730E+02, 2.17202E-01, 1.94337E+02, 2.15417E-02 },
    { 1.14130E+02, 7.08524E-02, 1.83451E+02, 1.51821E-01, 2.97581E+02, 2.21608E-01, 1.97617E+02, 2.19053E-02 },
    { 1.16554E+02, 7.22346E-02, 1.87962E+02, 1.54894E-01, 3.04516E+02, 2.26044E-01, 2.00924E+02, 2.22719E-02 },
    { 1.19007E+02, 7.36300E-02, 1.92529E+02, 1.57984E-01, 3.11536E+02, 2.30510E-01, 2.04259E+02, 2.26415E-02 },
    { 1.21488E+02, 7.50386E-02, 1.97152E+02, 1.61091E-01, 3.18640E+02, 2.35005E-01, 2.07621E+02, 2.30142E-02 },
    { 1.23997E+02, 7.64602E-02, 2.01830E+02, 1.64213E-01, 3.25827E+02, 2.39528E-01, 2.11011E+02, 2.33899E-02 },
    { 1.26534E+02, 7.78949E-02, 2.06563E+02, 1.67351E-01, 3.33098E+02, 2.44079E-01, 2.14428E+02, 2.37687E-02 },
    { 1.29100E+02, 7.93426E-02, 2.11351E+02, 1.70503E-01, 3.40451E+02, 2.48656E-01, 2.17873E+02, 2.41505E-02 },
    { 1.31695E+02, 8.08033E-02, 2.16192E+02, 1.73669E-01, 3.47887E+02, 2.53260E-01, 2.21345E+02, 2.45354E-02 },
    { 1.34318E+02, 8.22770E-02, 2.21087E+02, 1.76848E-01, 3.55405E+02, 2.57889E-01, 2.24844E+02, 2.49233E-02 },
    { 1.36969E+02, 8.37636E-02, 2.26035E+02, 1.80039E-01, 3.63004E+02, 2.62543E-01, 2.28371E+02, 2.53143E-02 },
    { 1.39649E+02, 8.52632E-02, 2.31036E+02, 1.83243E-01, 3.70685E+02, 2.67221E-01, 2.31925E+02, 2.57082E-02 },
    { 1.42358E+02, 8.67757E-02, 2.36089E+02, 1.86458E-01, 3.78447E+02, 2.71922E-01, 2.35507E+02, 2.61053E-02 },
    { 1.45095E+02, 8.83010E-02, 2.41195E+02, 1.89684E-01, 3.86289E+02, 2.76646E-01, 2.39117E+02, 2.65054E-02 },
    { 1.47860E+02, 8.98391E-02, 2.46351E+02, 1.92920E-01, 3.94211E+02, 2.81392E-01, 2.42753E+02, 2.69085E-02 },
    { 1.50655E+02, 9.13901E-02, 2.51558E+02, 1.96165E-01, 4.02213E+02, 2.86159E-01, 2.46417E+02, 2.73146E-02 },
    { 1.53478E+02, 9.29538E-02, 2.56816E+02, 1.99419E-01, 4.10293E+02, 2.90947E-01, 2.50109E+02, 2.77238E-02 },
    { 1.56329E+02, 9.45302E-02, 2.62123E+02, 2.02682E-01, 4.18453E+02, 2.95755E-01, 2.53828E+02, 2.81361E-02 },
    { 1.59209E+02, 9.61194E-02, 2.67480E+02, 2.05953E-01, 4.26690E+02, 3.00583E-01, 2.57574E+02, 2.85514E-02 },
    { 1.62118E+02, 9.77213E-02, 2.72886E+02, 2.09231E-01, 4.35005E+02, 3.05429E-01, 2.61348E+02, 2.89697E-02 },
    { 1.65056E+02, 9.93358E-02, 2.78340E+02, 2.12515E-01, 4.43397E+02, 3.10294E-01, 2.65150E+02, 2.93911E-02 },
    { 1.68023E+02, 1.00963E-01, 2.83842E+02, 2.15806E-01, 4.51865E+02, 3.15175E-01, 2.68979E+02, 2.98155E-02 },
    { 1.71018E+02, 1.02603E-01, 2.89392E+02, 2.19103E-01, 4.60409E+02, 3.20074E-01, 2.72835E+02, 3.02429E-02 },
    { 1.74042E+02, 1.04255E-01, 2.94988E+02, 2.22404E-01, 4.69029E+02, 3.24989E-01, 2.76719E+02, 3.06734E-02 },
    { 1.77094E+02, 1.05920E-01, 3.00630E+02, 2.25710E-01, 4.77724E+02, 3.29920E-01, 2.80630E+02, 3.11070E-02 },
    { 1.80176E+02, 1.07597E-01, 3.06318E+02, 2.29020E-01, 4.86494E+02, 3.34866E-01, 2.84568E+02, 3.15436E-02 },
    { 1.83286E+02, 1.09287E-01, 3.12051E+02, 2.32334E-01, 4.95337E+02, 3.39826E-01, 2.88534E+02, 3.19832E-02 },
    { 1.86425E+02, 1.10989E-01, 3.17828E+02, 2.35651E-01, 5.04254E+02, 3.44800E-01, 2.92528E+02, 3.24259E-02 },
    { 1.89593E+02, 1.12704E-01, 3.23650E+02, 2.38971E-01, 5.13243E+02, 3.49788E-01, 2.96549E+02, 3.28716E-02 },
    { 1.92790E+02, 1.14431E-01, 3.29515E+02, 2.42292E-01, 5.22305E+02, 3.54788E-01, 3.00597E+02, 3.33203E-02 },
    { 1.96016E+02, 1.16171E-01, 3.35423E+02, 2.45616E-01, 5.31438E+02, 3.59801E-01, 3.04673E+02, 3.37721E-02 },
    { 1.99270E+02, 1.17922E-01, 3.41373E+02, 2.48940E-01, 5.40643E+02, 3.64825E-01, 3.08776E+02, 3.42270E-02 },
    { 2.02554E+02, 1.19687E-01, 3.47364E+02, 2.52266E-01, 5.49918E+02, 3.69861E-01, 3.12907E+02, 3.46848E-02 },
    { 2.05866E+02, 1.21463E-01, 3.53397E+02, 2.55592E-01, 5.59263E+02, 3.74907E-01, 3.17065E+02, 3.51458E-02 },
    { 2.09207E+02, 1.23252E-01, 3.59470E+02, 2.58918E-01, 5.68678E+02, 3.79963E-01, 3.21251E+02, 3.56097E-02 },
    { 2.12577E+02, 1.25053E-01, 3.65584E+02, 2.62244E-01, 5.78161E+02, 3.85030E-01, 3.25464E+02, 3.60767E-02 },
    { 2.15976E+02, 1.26866E-01, 3.71736E+02, 2.65568E-01, 5.87712E+02, 3.90105E-01, 3.29705E+02, 3.65468E-02 },
    { 2.19404E+02, 1.28691E-01, 3.77928E+02, 2.68892E-01, 5.97332E+02, 3.95189E-01, 3.33973E+02, 3.70199E-02 },
    { 2.22861E+02, 1.30528E-01, 3.84157E+02, 2.72215E-01, 6.07018E+02, 4.00282E-01, 3.38268E+02, 3.74960E-02 },
    { 2.26346E+02, 1.32378E-01, 3.90424E+02, 2.75535E-01, 6.16771E+02, 4.05382E-01, 3.42591E+02, 3.79752E-02 },
    { 2.29861E+02, 1.34240E-01, 3.96728E+02, 2.78853E-01, 6.26589E+02, 4.10490E-01, 3.46941E+02, 3.84574E-02 },
    { 2.33405E+02, 1.36113E-01, 4.03069E+02, 2.82169E-01, 6.36473E+02, 4.15605E-01, 3.51319E+02, 3.89427E-02 },
    { 2.36977E+02, 1.37999E-01, 4.09445E+02, 2.85482E-01, 6.46422E+02, 4.20727E-01, 3.55724E+02, 3.94310E-02 },
    { 2.40578E+02, 1.39897E-01, 4.15856E+02, 2.88792E-01, 6.56435E+02, 4.25855E-01, 3.60157E+02, 3.99223E-02 },
    { 2.44209E+02, 1.41807E-01, 4.22302E+02, 2.92099E-01, 6.66511E+02, 4.30988E-01, 3.64617E+02, 4.04167E-02 },
    { 2.47868E+02, 1.43728E-01, 4.28783E+02, 2.95402E-01, 6.76651E+02, 4.36127E-01, 3.69105E+02, 4.09141E-02 },
    { 2.51557E+02, 1.45662E-01, 4.35296E+02, 2.98701E-01, 6.86853E+02, 4.41272E-01, 3.73620E+02, 4.14146E-02 },
    { 2.55274E+02, 1.47608E-01, 4.41842E+02, 3.01996E-01, 6.97116E+02, 4.46421E-01, 3.78162E+02, 4.19181E-02 },
    { 2.59020E+02, 1.49565E-01, 4.48421E+02, 3.05287E-01, 7.07441E+02, 4.51574E-01, 3.82732E+02, 4.24247E-02 },
    { 2.62795E+02, 1.51534E-01, 4.55031E+02, 3.08573E-01, 7.17827E+02, 4.56732E-01, 3.87329E+02, 4.29343E-02 },
    { 2.66599E+02, 1.53516E-01, 4.61673E+02, 3.11854E-01, 7.28272E+02, 4.61893E-01, 3.91954E+02, 4.34469E-02 },
    { 2.70432E+02, 1.55509E-01, 4.68345E+02, 3.15131E-01, 7.38777E+02, 4.67058E-01, 3.96606E+02, 4.39626E-02 },
    { 2.74294E+02, 1.57513E-01, 4.75047E+02, 3.18402E-01, 7.49341E+02, 4.72227E-01, 4.01286E+02, 4.44814E-02 },
    { 2.78185E+02, 1.59530E-01, 4.81778E+02, 3.21668E-01, 7.59963E+02, 4.77398E-01, 4.05993E+02, 4.50031E-02 },
    { 2.82105E+02, 1.61558E-01, 4.88538E+02, 3.24928E-01, 7.70643E+02, 4.82572E-01, 4.10728E+02, 4.55279E-02 },
    { 2.86054E+02, 1.63598E-01, 4.95326E+02, 3.28183E-01, 7.81381E+02, 4.87749E-01, 4.15490E+02, 4.60558E-02 },
    { 2.90032E+02, 1.65650E-01, 5.02142E+02, 3.31432E-01, 7.92175E+02, 4.92927E-01, 4.20279E+02, 4.65867E-02 },
    { 2.94039E+02, 1.67713E-01, 5.08986E+02, 3.34676E-01, 8.03025E+02, 4.98108E-01, 4.25096E+02, 4.71206E-02 },
    { 2.98075E+02, 1.69788E-01, 5.15855E+02, 3.37913E-01, 8.13930E+02, 5.03291E-01, 4.29940E+02, 4.76576E-02 },
    { 3.02140E+02, 1.71874E-01, 5.22751E+02, 3.41144E-01, 8.24891E+02, 5.08475E-01, 4.34812E+02, 4.81976E-02 },
    { 3.06234E+02, 1.73973E-01, 5.29672E+02, 3.44369E-01, 8.35906E+02, 5.13661E-01, 4.39711E+02, 4.87407E-02 },
    { 3.10356E+02, 1.76082E-01, 5.36619E+02, 3.47588E-01, 8.46975E+02, 5.18848E-01, 4.44638E+02, 4.92868E-02 },
    { 3.14508E+02, 1.78204E-01, 5.43589E+02, 3.50801E-01, 8.58097E+02, 5.24036E-01, 4.49592E+02, 4.98360E-02 },
    { 3.18689E+02, 1.80336E-01, 5.50584E+02, 3.54008E-01, 8.69273E+02, 5.29225E-01, 4.54574E+02, 5.03882E-02 },
    { 3.22898E+02, 1.82481E-01, 5.57602E+02, 3.57208E-01, 8.80500E+02, 5.34415E-01, 4.59583E+02, 5.09434E-02 },
    { 3.27137E+02, 1.84637E-01, 5.64643E+02, 3.60401E-01, 8.91779E+02, 5.39605E-01, 4.64619E+02, 5.15017E-02 },
    { 3.31404E+02, 1.86804E-01, 5.71706E+02, 3.63589E-01, 9.03110E+02, 5.44797E-01, 4.69683E+02, 5.20630E-02 },
    { 3.35701E+02, 1.88983E-01, 5.78791E+02, 3.66770E-01, 9.14491E+02, 5.49988E-01, 4.74775E+02, 5.26274E-02 },
    { 3.40026E+02, 1.91173E-01, 5.85897E+02, 3.69944E-01, 9.25923E+02, 5.55180E-01, 4.79894E+02, 5.31948E-02 },
    { 3.44381E+02, 1.93374E-01, 5.93024E+02, 3.73113E-01, 9.37405E+02, 5.60373E-01, 4.85040E+02, 5.37652E-02 },
    { 3.48764E+02, 1.95587E-01, 6.00172E+02, 3.76275E-01, 9.48935E+02, 5.65566E-01, 4.90214E+02, 5.43387E-02 },
    { 3.53176E+02, 1.97811E-01, 6.07339E+02, 3.79430E-01, 9.60515E+02, 5.70759E-01, 4.95415E+02, 5.49152E-02 },
    { 3.57617E+02, 2.00047E-01, 6.14526E+02, 3.82580E-01, 9.72143E+02, 5.75952E-01, 5.00643E+02, 5.54948E-02 },
    { 3.62087E+02, 2.02294E-01, 6.21732E+02, 3.85723E-01, 9.83819E+02, 5.81145E-01, 5.05899E+02, 5.60774E-02 },
    { 3.66586E+02, 2.04552E-01, 6.28956E+02, 3.88861E-01, 9.95542E+02, 5.86339E-01, 5.11183E+02, 5.66631E-02 },
    { 3.71114E+02, 2.06821E-01, 6.36198E+02, 3.91992E-01, 1.00731E+03, 5.91532E-01, 5.16494E+02, 5.72518E-02 },
    { 3.75671E+02, 2.09102E-01, 6.43458E+02, 3.95117E-01, 1.01913E+03, 5.96726E-01, 5.21832E+02, 5.78436E-02 },
    { 3.80257E+02, 2.11394E-01, 6.50736E+02, 3.98237E-01, 1.03099E+03, 6.01920E-01, 5.27198E+02, 5.84383E-02 },
    { 3.84871E+02, 2.13697E-01, 6.58029E+02, 4.01351E-01, 1.04290E+03, 6.07113E-01, 5.32591E+02, 5.90362E-02 },
    { 3.89515E+02, 2.16011E-01, 6.65340E+02, 4.04459E-01, 1.05485E+03, 6.12307E-01, 5.38012E+02, 5.96370E-02 },
    { 3.94187E+02, 2.18337E-01, 6.72666E+02, 4.07562E-01, 1.06685E+03, 6.17502E-01, 5.43460E+02, 6.02410E-02 },
    { 3.98888E+02, 2.20673E-01, 6.80007E+02, 4.10659E-01, 1.07890E+03, 6.22696E-01, 5.48936E+02, 6.08479E-02 },
    { 4.03618E+02, 2.23021E-01, 6.87364E+02, 4.13751E-01, 1.09098E+03, 6.27891E-01, 5.54439E+02, 6.14579E-02 },
    { 4.08377E+02, 2.25380E-01, 6.94736E+02, 4.16839E-01, 1.10311E+03, 6.33086E-01, 5.59970E+02, 6.20710E-02 },
    { 4.13165E+02, 2.27750E-01, 7.02121E+02, 4.19921E-01, 1.11529E+03, 6.38282E-01, 5.65528E+02, 6.26870E-02 },
    { 4.17982E+02, 2.30131E-01, 7.09521E+02, 4.22999E-01, 1.12750E+03, 6.43478E-01, 5.71113E+02, 6.33062E-02 },
    { 4.22828E+02, 2.32523E-01, 7.16935E+02, 4.26072E-01, 1.13976E+03, 6.48675E-01, 5.76726E+02, 6.39283E-02 },
    { 4.27702E+02, 2.34926E-01, 7.24361E+02, 4.29141E-01, 1.15206E+03, 6.53873E-01, 5.82366E+02, 6.45536E-02 },
    { 4.32606E+02, 2.37341E-01, 7.31801E+02, 4.32205E-01, 1.16441E+03, 6.59071E-01, 5.88034E+02, 6.51818E-02 },
    { 4.37538E+02, 2.39766E-01, 7.39253E+02, 4.35266E-01, 1.17679E+03, 6.64270E-01, 5.93729E+02, 6.58131E-02 },
    { 4.42499E+02, 2.42202E-01, 7.46717E+02, 4.38323E-01, 1.18922E+03, 6.69471E-01, 5.99452E+02, 6.64474E-02 },
    { 4.47488E+02, 2.44649E-01, 7.54193E+02, 4.41376E-01, 1.20168E+03, 6.74672E-01, 6.05202E+02, 6.70848E-02 },
    { 4.52507E+02, 2.47107E-01, 7.61681E+02, 4.44426E-01, 1.21419E+03, 6.79876E-01, 6.10979E+02, 6.77253E-02 },
    { 4.57555E+02, 2.49577E-01, 7.69180E+02, 4.47473E-01, 1.22673E+03, 6.85080E-01, 6.16784E+02, 6.83687E-02 },
    { 4.62631E+02, 2.52057E-01, 7.76690E+02, 4.50516E-01, 1.23932E+03, 6.90286E-01, 6.22617E+02, 6.90152E-02 },
    { 4.67736E+02, 2.54547E-01, 7.84211E+02, 4.53557E-01, 1.25195E+03, 6.95495E-01, 6.28477E+02, 6.96648E-02 },
    { 4.72870E+02, 2.57049E-01, 7.91743E+02, 4.56596E-01, 1.26461E+03, 7.00705E-01, 6.34364E+02, 7.03174E-02 },
    { 4.78032E+02, 2.59562E-01, 7.99284E+02, 4.59632E-01, 1.27732E+03, 7.05917E-01, 6.40279E+02, 7.09730E-02 },
    { 4.83224E+02, 2.62086E-01, 8.06835E+02, 4.62667E-01, 1.29006E+03, 7.11132E-01, 6.46221E+02, 7.16317E-02 },
    { 4.88444E+02, 2.64620E-01, 8.14396E+02, 4.65699E-01, 1.30284E+03, 7.16349E-01, 6.52191E+02, 7.22934E-02 },
    { 4.93693E+02, 2.67165E-01, 8.21967E+02, 4.68730E-01, 1.31566E+03, 7.21569E-01, 6.58188E+02, 7.29582E-02 },
    { 4.98971E+02, 2.69721E-01, 8.29546E+02, 4.71760E-01, 1.32852E+03, 7.26792E-01, 6.64212E+02, 7.36260E-02 },
    { 5.04277E+02, 2.72288E-01, 8.37135E+02, 4.74789E-01, 1.34141E+03, 7.32019E-01, 6.70265E+02, 7.42968E-02 },
    { 5.09612E+02, 2.74866E-01, 8.44732E+02, 4.77817E-01, 1.35434E+03, 7.37248E-01, 6.76344E+02, 7.49707E-02 },
    { 5.14976E+02, 2.77454E-01, 8.52338E+02, 4.80845E-01, 1.36731E+03, 7.42481E-01, 6.82451E+02, 7.56477E-02 },
    { 5.20369E+02, 2.80053E-01, 8.59952E+02, 4.83872E-01, 1.38032E+03, 7.47719E-01, 6.88585E+02, 7.63276E-02 },
    { 5.25790E+02, 2.82663E-01, 8.67574E+02, 4.86899E-01, 1.39336E+03, 7.52960E-01, 6.94747E+02, 7.70106E-02 },
    { 5.31240E+02, 2.85284E-01, 8.75204E+02, 4.89927E-01, 1.40644E+03, 7.58205E-01, 7.00936E+02, 7.76967E-02 },
    { 5.36719E+02, 2.87915E-01, 8.82842E+02, 4.92955E-01, 1.41956E+03, 7.63456E-01, 7.07153E+02, 7.83858E-02 },
    { 5.42227E+02, 2.90557E-01, 8.90488E+02, 4.95985E-01, 1.43271E+03, 7.68710E-01, 7.13397E+02, 7.90780E-02 },
    { 5.47763E+02, 2.93210E-01, 8.98141E+02, 4.99015E-01, 1.44590E+03, 7.73970E-01, 7.19669E+02, 7.97731E-02 },
    { 5.53328E+02, 2.95874E-01, 9.05801E+02, 5.02047E-01, 1.45913E+03, 7.79236E-01, 7.25968E+02, 8.04714E-02 },
    { 5.58921E+02, 2.98548E-01, 9.13468E+02, 5.05080E-01, 1.47239E+03, 7.84507E-01, 7.32294E+02, 8.11726E-02 },
    { 5.64544E+02, 3.01232E-01, 9.21143E+02, 5.08115E-01, 1.48569E+03, 7.89783E-01, 7.38648E+02, 8.18770E-02 },
    { 5.70195E+02, 3.03928E-01, 9.28824E+02, 5.11153E-01, 1.49902E+03, 7.95066E-01, 7.45030E+02, 8.25843E-02 },
    { 5.75874E+02, 3.06634E-01, 9.36512E+02, 5.14193E-01, 1.51239E+03, 8.00355E-01, 7.51438E+02, 8.32947E-02 },
    { 5.81582E+02, 3.09350E-01, 9.44207E+02, 5.17236E-01, 1.52579E+03, 8.05651E-01, 7.57875E+02, 8.40082E-02 },
    { 5.87319E+02, 3.12078E-01, 9.51908E+02, 5.20282E-01, 1.53923E+03, 8.10954E-01, 7.64338E+02, 8.47246E-02 },
    { 5.93085E+02, 3.14816E-01, 9.59616E+02, 5.23332E-01, 1.55270E+03, 8.16264E-01, 7.70830E+02, 8.54442E-02 },
    { 5.98879E+02, 3.17564E-01, 9.67330E+02, 5.26385E-01, 1.56621E+03, 8.21581E-01, 7.77348E+02, 8.61667E-02 },
    { 6.04701E+02, 3.20323E-01, 9.75051E+02, 5.29442E-01, 1.57975E+03, 8.26906E-01, 7.83894E+02, 8.68923E-02 },
    { 6.10553E+02, 3.23092E-01, 9.82777E+02, 5.32503E-01, 1.59333E+03, 8.32240E-01, 7.90468E+02, 8.76210E-02 },
    { 6.16433E+02, 3.25872E-01, 9.90510E+02, 5.35568E-01, 1.60694E+03, 8.37581E-01, 7.97069E+02, 8.83527E-02 },
    { 6.22341E+02, 3.28663E-01, 9.98250E+02, 5.38639E-01, 1.62059E+03, 8.42932E-01, 8.03697E+02, 8.90874E-02 }
  };
  const double x[] = {
    1.396263402e-5, 8.377720036e-1, 1.675530045e+0, 2.513288086e+0,
    3.351046126e+0, 4.188804167e+0, 5.026562208e+0, 5.864320249e+0,
    6.702078290e+0, 7.539836331e+0, 8.377594372e+0, 9.215352413e+0,
    1.005311045e+1, 1.089086850e+1, 1.172862654e+1, 1.256638458e+1,
    1.340414262e+1, 1.424190066e+1, 1.507965870e+1, 1.591741674e+1,
    1.675517478e+1, 1.759293282e+1, 1.843069086e+1, 1.926844890e+1,
    2.010620695e+1, 2.094396499e+1, 2.178172303e+1, 2.261948107e+1,
    2.345723911e+1, 2.429499715e+1, 2.513275519e+1, 2.597051323e+1,
    2.680827127e+1, 2.764602931e+1, 2.848378736e+1, 2.932154540e+1,
    3.015930344e+1, 3.099706148e+1, 3.183481952e+1, 3.267257756e+1,
    3.351033560e+1, 3.434809364e+1, 3.518585168e+1, 3.602360972e+1,
    3.686136776e+1, 3.769912581e+1, 3.853688385e+1, 3.937464189e+1,
    4.021239993e+1, 4.105015797e+1, 4.188791601e+1, 4.272567405e+1,
    4.356343209e+1, 4.440119013e+1, 4.523894817e+1, 4.607670622e+1,
    4.691446426e+1, 4.775222230e+1, 4.858998034e+1, 4.942773838e+1,
    5.026549642e+1, 5.110325446e+1, 5.194101250e+1, 5.277877054e+1,
    5.361652858e+1, 5.445428662e+1, 5.529204467e+1, 5.612980271e+1,
    5.696756075e+1, 5.780531879e+1, 5.864307683e+1, 5.948083487e+1,
    6.031859291e+1, 6.115635095e+1, 6.199410899e+1, 6.283186703e+1,
    6.366962508e+1, 6.450738312e+1, 6.534514116e+1, 6.618289920e+1,
    6.702065724e+1, 6.785841528e+1, 6.869617332e+1, 6.953393136e+1,
    7.037168940e+1, 7.120944744e+1, 7.204720548e+1, 7.288496353e+1,
    7.372272157e+1, 7.456047961e+1, 7.539823765e+1, 7.623599569e+1,
    7.707375373e+1, 7.791151177e+1, 7.874926981e+1, 7.958702785e+1,
    8.042478589e+1, 8.126254394e+1, 8.210030198e+1, 8.293806002e+1,
    8.377581806e+1, 8.461357610e+1, 8.545133414e+1, 8.628909218e+1,
    8.712685022e+1, 8.796460826e+1, 8.880236630e+1, 8.964012435e+1,
    9.047788239e+1, 9.131564043e+1, 9.215339847e+1, 9.299115651e+1,
    9.382891455e+1, 9.466667259e+1, 9.550443063e+1, 9.634218867e+1,
    9.717994671e+1, 9.801770475e+1, 9.885546280e+1, 9.969322084e+1,
    1.005309789e+2, 1.013687369e+2, 1.022064950e+2, 1.030442530e+2,
    1.038820110e+2, 1.047197691e+2, 1.055575271e+2, 1.063952852e+2,
    1.072330432e+2, 1.080708012e+2, 1.089085593e+2, 1.097463173e+2,
    1.105840754e+2, 1.114218334e+2, 1.122595915e+2, 1.130973495e+2,
    1.139351075e+2, 1.147728656e+2, 1.156106236e+2, 1.164483817e+2,
    1.172861397e+2, 1.181238977e+2, 1.189616558e+2, 1.197994138e+2,
    1.206371719e+2, 1.214749299e+2, 1.223126879e+2, 1.231504460e+2,
    1.239882040e+2, 1.248259621e+2, 1.256637201e+2, 1.265014781e+2,
    1.273392362e+2, 1.281769942e+2, 1.290147523e+2, 1.298525103e+2,
    1.306902684e+2, 1.315280264e+2, 1.323657844e+2, 1.332035425e+2,
    1.340413005e+2, 1.348790586e+2, 1.357168166e+2, 1.365545746e+2,
    1.373923327e+2, 1.382300907e+2, 1.390678488e+2, 1.399056068e+2,
    1.407433648e+2, 1.415811229e+2, 1.424188809e+2, 1.432566390e+2,
    1.440943970e+2, 1.449321550e+2, 1.457699131e+2, 1.466076711e+2,
    1.474454292e+2, 1.482831872e+2, 1.491209453e+2, 1.499587033e+2,
    1.507964613e+2, 1.516342194e+2, 1.524719774e+2, 1.533097355e+2,
    1.541474935e+2, 1.549852515e+2, 1.558230096e+2, 1.566607676e+2,
    1.574985257e+2, 1.583362837e+2, 1.591740417e+2, 1.600117998e+2,
    1.608495578e+2, 1.616873159e+2, 1.625250739e+2, 1.633628319e+2,
    1.642005900e+2, 1.650383480e+2, 1.658761061e+2, 1.667138641e+2,
    1.675516222e+2, 1.683893802e+2, 1.692271382e+2, 1.700648963e+2,
    1.709026543e+2, 1.717404124e+2, 1.725781704e+2, 1.734159284e+2,
    1.742536865e+2, 1.750914445e+2, 1.759292026e+2, 1.767669606e+2,
    1.776047186e+2, 1.784424767e+2, 1.792802347e+2, 1.801179928e+2,
    1.809557508e+2, 1.817935089e+2, 1.826312669e+2, 1.834690249e+2,
    1.843067830e+2, 1.851445410e+2, 1.859822991e+2, 1.868200571e+2,
    1.876578151e+2, 1.884955732e+2, 1.893333312e+2, 1.901710893e+2,
    1.910088473e+2, 1.918466053e+2, 1.926843634e+2, 1.935221214e+2,
    1.943598795e+2, 1.951976375e+2, 1.960353955e+2, 1.968731536e+2,
    1.977109116e+2, 1.985486697e+2, 1.993864277e+2, 2.002241858e+2,
    2.010619438e+2, 2.018997018e+2, 2.027374599e+2
  };
  struct s3d_device* s3d = NULL;
  struct s3d_shape* shape = NULL;
  struct geometry geometry;
  struct cylinder cylinder;
  const size_t nx = sizeof(x)/sizeof(double);
  const size_t nscatt_angles = 1000;
  const size_t ngeoms = 100;
  const size_t ndirs = 100;
  const double aspect_ratio = 0.837; /* diameter / height */
  double cylinder_volume = 0;
  size_t i;
  (void)argc, (void)argv;

  mem_init_proxy_allocator(&allocator, &mem_default_allocator);

  CHK(s3d_device_create(NULL, &allocator, 0, &s3d) == RES_OK);
  CHK(s3d_shape_create_mesh(s3d, &shape) == RES_OK);

  CHK(ssp_rng_create(&allocator, SSP_RNG_THREEFRY, &rng) == RES_OK);
  CHK(sschiff_device_create
    (NULL, &allocator, SSCHIFF_NTHREADS_DEFAULT, 1, s3d, &dev) == RES_OK);

  geometry_init_cylinder(&geometry, 64);
  cylinder.geometry = &geometry;
  cylinder.radius = 1.0;
  cylinder.height = (float)(cylinder.radius / aspect_ratio * 2.0);
  cylinder_setup_s3d_shape(&cylinder, shape);
  cylinder_volume = PI * cylinder.radius * cylinder.radius * cylinder.height;

  FOR_EACH(i, 0, nx) {
    const double wavelength = 0.6; /* In micron */
    struct sschiff_cross_section cross_section;
    double interval[2];
    struct sschiff_state* val;
    struct sschiff_state result;

    sampler_ctx.mean_radius = (x[i] * 0.450) / (2*PI);
    sampler_ctx.sigma = 1.18;
    sampler_ctx.shape = shape;
    sampler_ctx.cylinder_volume = cylinder_volume;

    distrib.characteristic_length = sampler_ctx.mean_radius;
    distrib.material.get_property = get_material_property;
    distrib.material.material = &sampler_ctx;
    distrib.sample = sample_cylinder;
    distrib.sample_volume_scaling = sample_volume_scaling;
    distrib.context = &sampler_ctx;

    time_current(&t0);
    CHK(sschiff_integrate(dev, rng, &distrib, &wavelength, 1,
      sschiff_uniform_scattering_angles, nscatt_angles, ngeoms, ndirs, 0,
      &estimator) == RES_OK);
    time_current(&t1);
    time_sub(&t0, &t1, &t0);
    time_dump(&t0, TIME_MIN|TIME_SEC|TIME_MSEC, NULL, buf, sizeof(buf));

    CHK(sschiff_estimator_get_cross_section
        (estimator, 0, &cross_section) == RES_OK);

    printf("%u - x = %g - Wavelength = %g micron - %s\n",
      (unsigned)i, x[i], wavelength, buf);

    val = &cross_section.extinction;
    result.E = results[i].extinction_E;
    result.SE = results[i].extinction_SE;
    compute_estimation_intersection(interval, 4, &result, val);
    printf("  Extinction ~ %9.3g +/- %9.3g ~ %9.3g +/- %9.3g (%9.3g)\n",
      results[i].extinction_E, results[i].extinction_SE,
      val->E, val->SE, interval[1] - interval[0]);
    CHK(interval[0] <= interval[1]);

    val = &cross_section.absorption;
    result.E = results[i].absorption_E;
    result.SE = results[i].absorption_SE;
    compute_estimation_intersection(interval, 4, &result, val);
    printf("  Absorption ~ %9.3g +/- %9.3g ~ %9.3g +/- %9.3g (%9.3g)\n",
      results[i].absorption_E, results[i].absorption_SE,
      val->E, val->SE, interval[1] - interval[0]);
    CHK(interval[0] <= interval[1]);

    val = &cross_section.scattering;
    result.E = results[i].scattering_E;
    result.SE = results[i].scattering_SE;
    compute_estimation_intersection(interval, 4, &result, val);
    printf("  Scattering ~ %9.3g +/- %9.3g ~ %9.3g +/- %9.3g (%9.3g)\n",
      results[i].scattering_E, results[i].scattering_SE,
      val->E, val->SE, interval[1] - interval[0]);
    CHK(interval[0] <= interval[1]);

    val = &cross_section.average_projected_area;
    result.E = results[i].avg_proj_area_E;
    result.SE = results[i].avg_proj_area_SE;
    compute_estimation_intersection(interval, 4, &result, val);
    printf("  Proj Area  ~ %9.3g +/- %9.3g ~ %9.3g +/- %9.3g (%9.3g)\n",
      results[i].avg_proj_area_E, results[i].avg_proj_area_SE,
      val->E, val->SE, interval[1] - interval[0]);
    CHK(interval[0] <= interval[1]);

    CHK(sschiff_estimator_ref_put(estimator) == RES_OK);
  }

  CHK(sschiff_device_ref_put(dev) == RES_OK);
  CHK(ssp_rng_ref_put(rng) == RES_OK);

  CHK(s3d_device_ref_put(s3d) == RES_OK);
  CHK(s3d_shape_ref_put(shape) == RES_OK);

  geometry_release(&geometry);
  check_memory_allocator(&allocator);
  mem_shutdown_proxy_allocator(&allocator);
  CHK(mem_allocated_size() == 0);
  return 0;
}

